package doug.dao;

import doug.model.Revenue;
import doug.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author ShawnCruz
 */
public class RevenueDAO {
    
    private Connection connection;

    public RevenueDAO() {
        connection = DatabaseUtility.getConnection();
    }
    
    public Revenue getRevenueByItem(String itemName) {
        
        Revenue revByItem = new Revenue();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT Item.ItemName AS Item, SUM(Auction.CurrentBid) AS Revenue\n" +
                                    "FROM Item, Auction, Customer\n" +
                                    "WHERE Item.ItemName = ? AND Auction.Finished = True AND Auction.ItemID = Item.ItemID AND \n" +
                                    "Auction.CurrentWinner = Customer.CustomerID;");

            preparedStatement.setString(1, itemName);
            ResultSet rs = preparedStatement.executeQuery();
            
            while (rs.next()) {
                revByItem.setName(rs.getString("Item"));
                revByItem.setRevenue(rs.getDouble("Revenue"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return revByItem;
    }
    
    public Revenue getRevenueByItemType(String itemType) {
        
        Revenue revByItemType = new Revenue();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT Item.ItemType AS ItemType, SUM(Auction.CurrentBid) AS Revenue\n" +
                                    "FROM Item, Auction, Customer\n" +
                                    "WHERE Item.ItemType = ? AND Auction.Finished = True AND \n" +
                                    "Auction.ItemID = Item.ItemID AND Auction.CurrentWinner = Customer.CustomerID;");

            preparedStatement.setString(1, itemType);
            ResultSet rs = preparedStatement.executeQuery();
            
            while (rs.next()) {
                revByItemType.setName(rs.getString("ItemType"));
                revByItemType.setRevenue(rs.getDouble("Revenue"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return revByItemType;
    }
    
    public Revenue getRevenueByCustomer(String customerName) {
        
        Revenue revByCustomer = new Revenue();
        String firstName = customerName.split(" ")[0];
        String lastName = customerName.split(" ")[1];
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT User.UserID, User.FirstName AS SellerFirstName, User.LastName AS SellerLastName, SUM(Auction.CurrentBid) AS Revenue\n" +
                                    "FROM User, Customer, Auction, Post\n" +
                                    "WHERE User.FirstName = ? AND User.LastName = ? AND \n" +
                                    "Post.AuctionID = Auction.AuctionID AND Post.SellerID = Customer.CustomerID AND \n" +
                                    "Auction.Finished = True  AND Customer.CustomerID = User.UserID;");

            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            ResultSet rs = preparedStatement.executeQuery();
            
            while (rs.next()) {
                revByCustomer.setName(rs.getString("SellerFirstName") + " " + rs.getString("SellerLastName"));
                revByCustomer.setRevenue(rs.getDouble("Revenue"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return revByCustomer;
    }   
    
    public Revenue getCustRepMaxRev() {
        
        Revenue custRepMax = new Revenue();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT User.FirstName AS CustRepFirstName, User.LastName AS CustRepLastName, SUM(Auction.CurrentBid) AS Revenue\n" +
                                    "FROM User, Auction, Employee, Post\n" +
                                    "WHERE Auction.Finished = True AND Auction.Monitor = Employee.EmployeeID AND\n" +
                                    "Employee.EmployeeID = User.UserID AND Employee.Lvl = 0 AND Auction.AuctionID = Post.AuctionID\n" +
                                    "GROUP BY Employee.EmployeeID\n" +
                                    "ORDER BY Revenue DESC\n" +
                                    "LIMIT 1;");
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while (rs.next()) {
                custRepMax.setName(rs.getString("CustRepFirstName") + " " + rs.getString("CustRepLastName"));
                custRepMax.setRevenue(rs.getDouble("Revenue"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return custRepMax;
    }  
    
    public Revenue getCustomerMaxRev() {
        
        Revenue custMax = new Revenue();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT User.FirstName AS CustomerFName, User.LastName AS CustomerLName,\n" +
                                    "SUM(Auction.CurrentBid) AS Revenue\n" +
                                    "FROM User, Auction, Customer, Post, Item\n" +
                                    "WHERE Auction.Finished = True  AND Customer.CustomerID = User.UserID AND\n" +
                                    "Auction.AuctionID = Post.AuctionID AND Item.ItemID = Auction.ItemID AND\n" +
                                    "Post.SellerID = Customer.CustomerID\n" +
                                    "GROUP BY Customer.CustomerID\n" +
                                    "ORDER BY Revenue DESC\n" +
                                    "LIMIT 1;");
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while (rs.next()) {
                custMax.setName(rs.getString("CustomerFName") + " " + rs.getString("CustomerLName"));
                custMax.setRevenue(rs.getDouble("Revenue"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return custMax;
    }  
    
}
