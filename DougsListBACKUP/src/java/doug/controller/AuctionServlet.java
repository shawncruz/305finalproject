package doug.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import doug.dao.AuctionDAO;
import doug.dao.ItemDAO;
import doug.dao.SalesDAO;
import doug.dao.UserDAO;
import doug.model.Auction;
import doug.model.Item;
import doug.model.Post;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ShawnCruz
 */
public class AuctionServlet extends HttpServlet {

    private static String CREATE_AUCTION = "/createAuction.jsp";
    private static String VIEW_AUCTION = "/viewAuction.jsp";
    private static String CREATE_ITEM = "/createItem.jsp";
    private static String LIST_AUCTIONS = "/listAuction.jsp";
    private static String ERROR = "/error.jsp";
    private static String LIST_AUCTIONS_CUSTOMER = "/listAuctionCustomer.jsp";
    private SalesDAO salesDao;
    private AuctionDAO auctionDao;
    private ItemDAO itemDao;
    private UserDAO userDao;

    public AuctionServlet() {
        super();
        salesDao = new SalesDAO();
        auctionDao = new AuctionDAO();
        itemDao = new ItemDAO();
        userDao = new UserDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("recordSale")) {

            int auctionId = Integer.parseInt(request.getParameter("auctionId"));
            salesDao.recordSale(auctionId);
            forward = LIST_AUCTIONS;
            request.setAttribute("auctions", salesDao.getAuctions());

        } else if (action.equalsIgnoreCase("listAuctions")) {

            forward = LIST_AUCTIONS;
            request.setAttribute("auctions", salesDao.getAuctions());
            request.getSession().setAttribute("auctions", salesDao.getAuctions());

        } else if (action.equalsIgnoreCase("listAuctionsCustomer")) {

            forward = LIST_AUCTIONS_CUSTOMER;
            request.setAttribute("auctions", salesDao.getAuctions());
            request.getSession().setAttribute("auctions", salesDao.getAuctions());

        } else if (action.equalsIgnoreCase("viewAuction")) {
            int auctionId = Integer.parseInt(request.getParameter("auctionId"));
            forward = VIEW_AUCTION;
            Auction auction = auctionDao.getAuctionById(auctionId);
            Post post = auctionDao.getPostById(auctionId);
            request.setAttribute("auction", auction);
            request.setAttribute("post",post);
            request.setAttribute("item",itemDao.getItemById(auction.getItemId()));
            request.setAttribute("seller",userDao.getUserById(post.getSellerId()));
            request.setAttribute("bids", auctionDao.getBidsByAuctionId(auctionId));
            request.getSession().setAttribute("auctions", salesDao.getAuctions());

        } else {
            forward = ERROR;
        }
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Create an auction and post
        Auction auction = new Auction();
        Post post = new Post();
        //Set user-specified variables for auction
        double bidIncrement = Double.parseDouble(request.getParameter("bidIncrement"));
        double reservePrice = Double.parseDouble(request.getParameter("reservePrice"));
        auction.setBidIncrement(bidIncrement);
        auction.setReservePrice(reservePrice);
        //Set variables for post
        post.setExpireDate(request.getParameter("expireDate"));
        String now = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
        post.setPostDate(now);
        post.setSellerId(Integer.parseInt(request.getParameter("sellerID")));
        //Determine item from user specified item name, get its id
        //and set auction item id to this value
        String itemName = request.getParameter("itemName");
        Item item = itemDao.getItemByName(itemName);
        //if the item id is not 0, the item already exists in the database
        if (item.getItemId() != 0) {
            itemDao.incrementAmountInStock(item.getItemId());
            auction.setItemId(item.getItemId());
            //using "auction =" here updates the auction id
            auction = auctionDao.addAuction(auction, post);
            //set attributes for view auction page
            request.setAttribute("auction", auction);
            request.setAttribute("item", item);
            request.setAttribute("post", post);
            request.setAttribute("seller", userDao.getUserById(post.getSellerId()));
            RequestDispatcher view = request.getRequestDispatcher(VIEW_AUCTION);
            view.forward(request, response);
        } //otherwise, we have to create the item, so go to the create item page 
        //(remember the item name/disable the field, so the user cannot modify 
        //it to an item that IS in the database after the fact)
        else {
            request.setAttribute("itemName", request.getParameter("itemName"));
            RequestDispatcher view = request.getRequestDispatcher(CREATE_ITEM);
            view.forward(request, response);
        }

    }

}
