INSERT INTO User (Password, LastName, FirstName, Address, City, 
State, ZipCode, Telephone, Email)
VALUES 
('password', 'Smith', 'David', '123 College Road', 
	'Stony Brook', 'NY', '11790', '(516)215-2345', 
	'dsmith@dougslist.com'), 
('password', 'Warren', 'David', '456 Sunken Street', 
	'Stony Brook', 'NY', '11790', '(516)632-9987', 
	'dwarren@dougslist.com'), 
('password', 'Lu', 'Shiyong', '123 Success Street', 
	'Stony Brook', 'NY', '11790', '(516)632-8959', 
	'shiyong@cs.sunysb.edu'), 
('password', 'Du', 'Haixia', '456 Fortune Road', 
	'Stony Brook', 'NY', '11790', '(516)632-4360', 
	'dhaixia@cs.sunysb.edu'), 
('password', 'Smith', 'John', '789 Peace Blvd', 
	'Los Angeles', 'CA', '12345', '(412)443-4321', 
	'dhaixia@cs.sunysb.edu'), 
('password', 'Phil', 'Lewis', '456 Fortune Road', 
	'Stony Brook', 'NY', '11790', '(516)666-8888', 
	'pml@cs.sunysb.edu');

INSERT INTO Employee (EmployeeID, StartDate, 
	HourlyRate, Lvl)
VALUES ('10000000', '1998-11-01', 60, 0),
	('10000001', '1999-2-2', 50, 1);

INSERT INTO Customer (CustomerID, Rating, CreditCardNum)
VALUES ('10000002', '1', '1234567812345678'),
('10000003', '1', '5678123456781234'),
('10000004', '1', '2345678923456789'),
('10000005', '1', '6789234567892345');

INSERT INTO Item (Description, ItemName, Year, ItemType,
AmountInStock, CopiesSold)
VALUES ('A wicked romantic film.', 'Titanic', '2005', 'DVD', 4, 1),
('A wicked affordable car', 'Nissan Sentra', '2007', 'Car', 1, 3);

INSERT INTO Auction(ItemID, Monitor, BidIncrement,
 CurrentBid, ReservePrice, Finished)
	VALUES ('1', '10000001', 1, 5, 10, FALSE),
	('2', '10000001', 10, 1000, 2000, FALSE);

INSERT INTO Post (SellerID, AuctionId, PostDate, ExpireDate)
VALUES ('10000005', '1', CURRENT_TIMESTAMP, 
	'2015-12-16 13:00:00'), 
	('10000004', '2', CURRENT_TIMESTAMP, 
		'2015-12-16 13:00:00');

INSERT INTO Bid (BidderId, BidPlaced, CurrentMaxBid,
	AuctionId, ItemID, BidTime)
VALUES ('10000003', 5, 10, '1', '1', '2015-12-04 11:00:00'),
('10000002', 9, 10, '1', '1', '2015-12-04 11:05:00'),
('10000002', 10, 10, '1', '1', '2015-12-04 11:06:00'),
('10000002', 11, 15, '1', '1', '2015-12-04 11:06:30');

UPDATE Auction 
SET CurrentWinner='10000002'
WHERE AuctionId=1;

UPDATE Auction
SET Finished=TRUE
WHERE AuctionId=1;


