<%-- 
    Document   : help
    Created on : Dec 2, 2015, 12:09:58 AM
    Author     : Lauren
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>  
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Doug's List - Help</title>  
    </head>  
    <body> 

        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>                    
                    <!--- Display "Log In or Sign Up" welcome message only if no user is currently logged in --->
                    <c:if test="${empty currentUser}">
                        <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                                <c:out value="Hello! Please <a href='login.jsp'>Log In</a> or <a href='registration.jsp'>Register</a> "  escapeXml="false"/>
                            </c:if>
                    <!--- Else, display welcome message containing user's name, user icon (to access user page), and logout button --->
                    <c:if test="${!empty currentUser}">

                        <c:out value="
                               <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                               <div class='account'><a href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                               <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                               Hello, ${currentUser.firstName}"  
                               escapeXml="false"/>

                    </c:if>  
                </div>
            </div>
        </nav>

        <div class='container'>
            <div class ='help-toc' id='help-toc'>
                <h4>Table of Contents</h4>
                <hr>
                <ul style="list-style-type:none">
                    <li>Customer Help</li>
                    <ol>
                        <li>Creating an Account</li>
                        <li>Logging into an Account</li>
                        <li>Creating an Auction</li>
                        <li>Viewing/Bidding on an Auction</li>
                        <li>Finding Available Items</li>
                    </ol>
                    <li>Customer Representative Help</li>
                    <ol>
                        <li>Viewing, Editing, and Adding Customers</li>
                        <li>Viewing Auctions and Recording Sales</li>
                        <li>Mailing List</li>
                        <li>Item Suggestions List</li>
                    </ol>
                    <li>Manager Help</li>
                    <ol>
                        <li>Viewing, Editing, and Adding Customers</li>
                        <li>Viewing, Editing, and Adding Employees</li>
                        <li>Viewing Items</li>
                        <li>Viewing Sales Reports</li>
                        <li>Viewing Revenue Reports</li>
                    </ol>
                </ul>  
            </div>
            <p></p>
            <!------ Customer Help Section -------------->
            <h4 id='custHelp'>Customer Help</h4>
            <hr>
            <p>
                Welcome to Doug's List! Doug's List is an online auction house where you can buy and sell items. 
                You must have an account to buy or sell items. You can reach this help page from any other page on the website
                by clicking the question mark icon on the top right. You can also get back to the main page at any time by clicking
                the home icon, or your account page by clicking on the "My Account" icon (it looks like the silhouette of a person). 
                The logout icon (looks like a square with an arrow pointing to the right) will allow you to log out at any time.
                For your safety, it is recommended that you log out at the end of each session. Please note that the "My Account" and
                "Login" buttons are only available to logged-in users, all other users will see a prompt to log in or register.
                If you are ever unsure of what a button does, try hovering over it.
            </p>
            <h6 id='accCreate'>Creating an Account</h6>
            <p>If you are not currently signed in to an account, there should be
                a welcome message at the top right of the screen with links to 
                <a href="login.jsp">Log In</a> or <a href="registration.jsp">Register.</a>
                To create an account, simply click "Register", fill out the form that appears on the page,
                and submit it. This will create an account and redirect you to the login page, where you can log in.</p>
            <h6 id='login'>Logging Into an Account</h6>
            <p>If you are not currently signed in to an account, there should be
                a welcome message at the top right of the screen with links to 
                <a href="login.jsp">Log In</a> or <a href="registration.jsp">Register.</a>
                To log into an account, simply click "Log In", and enter the E-Mail and 
                Password you selected when you registered.</p>
            <h6>Creating an Auction</h6>
            <p>If you are logged in, you should see a welcome message with your name at the top right of your screen
                alongside three icons. These icons lead, respectively, to the homepage, your account page, or logout. 
                Click the "My Account" button (when hovering over the button, you should see a tooltip that says "My Account").
                There are three main sections here. The top section lists some information about your account. You will want
                to keep this information current in order to have the best experience on the website. The second section, titled Auction Tools,
                is where you can create an auction. Click on the Create Auction link<br><br>

                This will take you to the create auction page, where you will be prompted to enter some information
                about the auction, such as the end time and reserve price (the price at which an item is automatically bought, 
                similar to eBay's "Buy it Now!") If the item you are selling exists in our database, your auction will be created
                and you will be redirected to the View Auction page. <br><br>

                If the item you are trying to list does not yet exist in the databse, you will be taken to the Create Item page, where
                you will be prompted to enter some more information about the item. After this, you will be able to create your auction.</p>
            <h6>Viewing/Bidding on an Auction</h6>
            <p>If you are logged in, you should see a welcome message with your name at the top right of your screen
                alongside three icons. These icons lead, respectively, to the homepage, your account page, or logout. 
                Click the "My Account" button (when hovering over the button, you should see a tooltip that says "My Account"). 
                The top section lists some information about your account. You will want
                to keep this information current in order to have the best experience on the website. The second section, titled Auction Tools,
                is where you can view an auction. Click on the List Auctions link.<br><br>

                This will take you to the auction list page, where you can view all currently ongoing auctions. In order to view and bid on an auction,
                click the View link in the action column. This will take you to the auction page, where you will be able to view information about the auction,
                including all current bids. You will also be able to bid on the item here, by placing a bid in the box labelled "Place a bid" and submitting it.
            </p>
            <h6>Finding Available Items</h6>
            <p>If you are logged in, you should see a welcome message with your name at the top right of your screen
                alongside three icons. These icons lead, respectively, to the homepage, your account page, or logout. 
                Click the "My Account" button (when hovering over the button, you should see a tooltip that says "My Account"). 
                The top section lists some information about your account. You will want
                to keep this information current in order to have the best experience on the website. The second section, titled Auction Tools,
                is where you can view or create an auction. The third section, titled Find Items, is where you can access information about available items.<br><br>

                There are four ways to view available items:
            <ul>
                <li>Best Sellers - This will take you to a list of the top three items that have sold the most on Doug's List</li>
                <li>By Seller - After entering a seller ID, this will take you to a list of the available items sold by a particular seller on Doug's List.</li>
                <li>By Type - After entering an Item Type (e.g. "DVD") this will take you to a list of the available items sold of a particular type.</li>
                <li>By Keyword - After entering a keyword, this will take you to a list of the available items sold that match a particular keyword</li>
            </ul>
        </p>
        <!----- Customer Rep Help Section ---------->
        <h4 id='repHelp'>Customer Representative Help</h4>
        <hr>
        <p>Welcome to Doug's List! As a customer representative, you have tools at your disposal to aid customers with buying and selling their items, 
            and having an overall positive experience on the website. If you are logged in, you should see a welcome message with your name at the top right of your screen
            alongside three icons. These icons lead, respectively, to the homepage, your account page, or logout. 
            Click the "My Account" button (when hovering over the button, you should see a tooltip that says "My Account").
            There are two main sections here. The top section lists some information about your account.
            The second section, titled Representative tools, is where you can find the tools you will be using.
            To learn how to best use the tools at your disposal, please review the following sections.</p>
        <h6>Viewing, Editing, and Adding Customers</h6>
        <p>
            Under Representative Tools, click on List Customers. This will take you a list of all customers that exist in the database. The Action column
            contains Update and Delete links, which will allow you to update or delete an customer accordingly. Add customer will allow you to add an customer.

            The add and update customer pages contain fields for relevenat  information. Every field must be filled out. If any information is left blank,
            the update or add will fail. Please note that Customer IDs are automatically generated and incremented by the database, and thus cannot be created, updated
            or otherwise edited. For existing customers, their customer ID will be visible but read-only.
        </p>
        <h6>Viewing Auctions and Recording Sales</h6>
        <p>
            Under Representative Tools, click on List Auctions. This will take you a list of all ongoing (unfinished) auctions that exist in the database. The Action column
            contains a view link, which will allow you to view the auction page. This page contains information about the auction and a list of current bids.<br><br>

            For customer representatives, the action column also contains a Record Sale link. Clicking on this will end the auction, marking it as finished,
            and record it as a sale.
        </p>
        <h6>Mailing List</h6>
        <p>
            Customer Representatives may wish to construct a mailing list for their customers. To do this, select the Mailing List link under Representative Tools.<br<br>
            On this page, you can enter a customer ID to add them to your mailing list, or click the remove button next to their entry in the list to delete them.
            The mailing list is useful if you need to get in touch with a customer or group of customers regarding an auction.

        </p>
        <h6>Item Suggestions List</h6>
        <p>
            You may wish to generate a list of item suggestions for a particular customer. To do this, enter their customer ID in the box under "Produce list of item suggestions for given customer"
            After clicking submit, you will be directed to a list of items based on previously completed auctions that the customer has participated in.
        </p>
        <!------- Manager Help Section -------------->
        <h4 id='managerHelp'>Manager Help</h4>         
        <hr>
        <p>Welcome to Doug's List! As a manager, you have tools at your disposal to aid customers with buying and selling their items,
            in addition to administrative tools to manage both employees and customers. If you are logged in, you should see a welcome message with your name at the top right of your screen
            alongside three icons. These icons lead, respectively, to the homepage, your account page, or logout. 
            Click the "My Account" button (when hovering over the button, you should see a tooltip that says "My Account"). Here, there are four sections.
            The top section lists some information about your account.  The remaining sections are where you can find the tools you will be using.          
        </p>
        <h6>Viewing, Editing, and Adding Customers</h6>
        <p>
            Under Manager Tools, click on List Customers. This will take you a list of all customers that exist in the database. The Action column
            contains Update and Delete links, which will allow you to update or delete an customer accordingly. Add customer will allow you to add an customer.

            The add and update customer pages contain fields for relevenat  information. Every field must be filled out. If any information is left blank,
            the update or add will fail. Please note that Customer IDs are automatically generated and incremented by the database, and thus cannot be created, updated
            or otherwise edited. For existing customers, their customer ID will be visible but read-only.
        </p>
        <h6>Viewing, Editing, and Adding Employees</h6>
        <p>
            Under Manager Tools, click on List Employees. This will take you a list of all employees that exist in the database. The Action column
            contains Update and Delete links, which will allow you to update or delete an employee accordingly. Add employee will allow you to add an employee.

            The add and update employee pages contain fields for relevant employee information. Every field must be filled out. If any information is left blank,
            the update or add will fail. Please note that Employee IDs are automatically generated and incremented by the database, and thus cannot be created, updated
            or otherwise edited. For existing employees, their employee ID will be visible but read-only.
        </p>
        <h6>Viewing Items</h6>
        <p>
            Under Manager Tools, click on List Items. This will take you a list of all items that exist in the database. This is a useful way to
            obtain a summary listing of all item information entered by customers, such as item ids, item types, and copies sold of each item.
        </p>
        <h6>Viewing Sales Reports</h6>
        <p>
            The Sales section contains all of the tools you need to determine how items are selling.
            <br><br>There are four types of sales report:</p>
        <ul>
            <li>Best Sellers - This will take you to a list of the top three items that have sold the most on Doug's List</li>
            <li>By Month - After entering a month, this will take you to a list sales for that month.</li>
            <li>By Item Name - After entering an Item Name, this will take you to a list of sales of items of a particular type.</li>
            <li>By Customer Name - After entering a customer name, this will take you to a list of all sales made by a particular customer</li>
        </ul><br>
        <h6>Viewing Revenue Reports</h6>
        <p>
            The Revenue section contains all of the tools you need to determine how much money is being generated.
            <br><br>There are five types of revenue report:
        <ul>
            <li>By Item Name - After entering an Item Name, this will take you to a list of revenue produced from sales of that particular item.</li>
            <li>By Item Type - After entering an Item Type, this will take you to a list of revenue produced from sales of that particular item type..</li>
            <li>By Customer Name - After entering a customer name, this will take you to a list of all revenue produced by sales of a particular customer</li>
            <li>Top-Performing Customer Rep - Click the button to see which customer representative has generated the most revenue</li>
            <li>Top-Selling Customer - Click the button to see which customer has generated the most revenue</li>
        </ul>

    </p>
</div>
</body>  
</html>  