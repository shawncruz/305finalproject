package doug.dao;

import doug.model.Customer;
import doug.model.User;
import doug.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ShawnCruz
 */
public class CustomerDAO {
    
    private Connection connection;
    private UserDAO userDAO;

    public CustomerDAO() {
        connection = DatabaseUtility.getConnection();
        userDAO = new UserDAO();
    }

    public void addCustomer(Customer customer) {
        try {
            User user = userDAO.addUser(customer);
            customer.setUserInfo(user);
            
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Customer(CustomerID, Rating, CreditCardNum) VALUES (?, ?, ?)");
            // Parameters start with 1
            preparedStatement.setInt(1, customer.getUserId());
            preparedStatement.setString(2, customer.getRating());
            preparedStatement.setString(3, customer.getCreditCard());        
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } 
    }

    public void deleteCustomer(int customerId) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM Customer WHERE CustomerID=?");
            // Parameters start with 1
            preparedStatement.setInt(1, customerId);
            preparedStatement.executeUpdate();
            userDAO.deleteUser(customerId);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateCustomer(Customer customer) {
        try {
            userDAO.updateUser(customer);
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Customer SET Rating=?, CreditCardNum=? WHERE CustomerID=?");

            preparedStatement.setString(1, customer.getRating());
            preparedStatement.setString(2, customer.getCreditCard());
            preparedStatement.setInt(3, customer.getCustomerId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Customer> getAllCustomers() {
        List<Customer> customers = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM Customer");
            while (rs.next()) {
                Customer customer = new Customer();
                int customerId = rs.getInt("CustomerID");
                customer.setCustomerId(customerId);
                customer.setRating(rs.getString("Rating"));
                customer.setCreditCard(rs.getString("CreditCardNum"));
                customer.setUserInfo(userDAO.getUserById(customerId));
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }
    
    public Customer getCustomerById(int customerId) {
        Customer customer = new Customer();
        try {
            User user = userDAO.getUserById(customerId);
            customer.setUserInfo(user);
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT * FROM Customer WHERE CustomerID=?");
            preparedStatement.setInt(1, customerId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                customer.setCustomerId(rs.getInt("CustomerID"));
                customer.setRating(rs.getString("Rating"));
                customer.setCreditCard(rs.getString("CreditCardNum"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }
    
}
