package doug.dao;

import doug.model.Employee;
import doug.model.User;
import doug.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ShawnCruz
 */
public class EmployeeDAO {
    
    private Connection connection;
    private UserDAO userDAO;

    public EmployeeDAO() {
        connection = DatabaseUtility.getConnection();
        userDAO = new UserDAO();
    }
    
    public void addEmployee(Employee employee) {
        try {
            User user = userDAO.addUser(employee);
            employee.setUserInfo(user);
            
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Employee(EmployeeID, StartDate, HourlyRate, Lvl) VALUES (?, ?, ?, ?)");
            // Parameters start with 1
            preparedStatement.setInt(1, employee.getUserId());
            preparedStatement.setString(2, employee.getStartDate());
            preparedStatement.setString(3, employee.getHourlyRate());
            preparedStatement.setInt(4, employee.getLvl());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void deleteEmployee(int employeeId) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM Employee WHERE EmployeeID=?");
            // Parameters start with 1
            preparedStatement.setInt(1, employeeId);
            preparedStatement.executeUpdate();
            userDAO.deleteUser(employeeId);
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void updateEmployee(Employee employee) {
        try {
            userDAO.updateUser(employee);
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Employee SET StartDate=?, HourlyRate=?, Lvl=? WHERE EmployeeID=?");
            // Parameters start with 1
            preparedStatement.setString(1, employee.getStartDate());
            preparedStatement.setString(2, employee.getHourlyRate());
            //preparedStatement.setDate(3, new java.sql.Date(user.getDob().getTime())); MAY NEED THIS LATER
            preparedStatement.setInt(3, employee.getLvl());
            preparedStatement.setInt(4, employee.getEmployeeId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List<Employee> getAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM Employee WHERE Employee.Lvl = 0");
            while (rs.next()) {
                Employee employee = new Employee();
                int employeeId = rs.getInt("EmployeeID");
                employee.setEmployeeId(employeeId);
                employee.setStartDate(rs.getString("StartDate"));
                employee.setHourlyRate(rs.getString("HourlyRate"));
                employee.setLvl(rs.getInt("Lvl"));
                employee.setUserInfo(userDAO.getUserById(employeeId));
                employees.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return employees;
    }
    
    public Employee getEmployeeById(int employeeId) {
        Employee employee = new Employee();
        try {
            User user = userDAO.getUserById(employeeId);
            employee.setUserInfo(user);
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT * FROM Employee WHERE EmployeeID=?");
            preparedStatement.setInt(1, employeeId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                employee.setEmployeeId(rs.getInt("EmployeeID"));
                employee.setStartDate(rs.getString("StartDate"));
                employee.setHourlyRate(rs.getString("HourlyRate"));
                employee.setLvl(rs.getInt("Lvl"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return employee;
    }
}
