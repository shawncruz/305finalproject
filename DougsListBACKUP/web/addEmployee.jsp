<%-- 
    Document   : addEmployee
    Created on : Nov 19, 2015, 7:20:09 PM
    Author     : ShawnCruz
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Add/Edit Employee</title>  
    </head>  
    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="registration">
                <form method="POST" action='EmployeeController' name="frmAddEmployee">
                    <fieldset style="width: 400px">  
                        <table>
                            <c:if test="${!empty employee.employeeId}">
                            <tr>
                                <td>Employee ID (Read-Only): </td>
                                <td><input type="text" readonly="readonly" name="employeeId" value="<c:out value="${employee.employeeId}" />"></td>
                            </tr>
                            </c:if>
                            <tr>
                                <td>Password: </td>
                                <td><input type="password" name="password" value="<c:out value="${employee.password}" />"> </td>
                            </tr>
                            <tr>
                                <td>First Name: </td>
                                <td><input type="text" name="firstName" value="<c:out value="${employee.firstName}" />"> </td>
                            </tr>
                            <tr>
                                <td>Last Name: </td>
                                <td><input type="text" name="lastName" value="<c:out value="${employee.lastName}" />"> </td>
                            </tr>
                            <tr>
                                <td>Address: </td>
                                <td>  <input type="text" name="address" value="<c:out value="${employee.address}" />"></td>
                            </tr>
                            <tr>
                                <td>City: </td>
                                <td> <input type="text" name="city" value="<c:out value="${employee.city}" />"> </td>
                            </tr>
                            <tr>
                                <td>State: </td>
                                <td><input type="text" name="state" value="<c:out value="${employee.state}" />"> </td>
                            </tr>
                            <tr>
                                <td>Zip Code : </td>
                                <td><input type="text" name="zipcode" value="<c:out value="${employee.zipcode}" />"></td>
                            </tr>
                            <tr>
                                <td>Telephone: </td>
                                <td>    <input type="text" name="telephone" value="<c:out value="${employee.telephone}" />"> </td>
                            </tr>
                            <tr>
                                <td>Email: </td>
                                <td><input type="text" name="email" value="<c:out value="${employee.email}" />"> </td>
                            </tr>
                            <tr>
                                <td>Start Date :</td>
                                <td><input type="text" name="startDate" value="<c:out value="${employee.startDate}" />"> </td>
                            </tr>
                            <tr>
                                <td>Hourly Rate : </td>
                                <td><input type="text" name="hourlyRate" value="<c:out value="${employee.hourlyRate}" />"> </td>
                            </tr>
                            <tr>
                                <td>Level : </td>
                                <td><input type="text" name="lvl" value="<c:out value="${employee.lvl}" />"> </td>
                            </tr>
                        </table>
                        <div class="submit-button">
                            <input type="submit" value="Submit">
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>
