/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doug.controller;

import doug.dao.AuctionDAO;
import doug.dao.ItemDAO;
import doug.dao.SalesDAO;
import doug.dao.UserDAO;
import doug.model.Auction;
import doug.model.Bid;
import doug.model.Item;
import doug.model.Post;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet handles addition/modification of items in the database
 *
 * @author Lauren
 */
public class BidServlet extends HttpServlet {

    private AuctionDAO auctionDao;
    private ItemDAO itemDao;
    private UserDAO userDao;
    
    private static String VIEW_AUCTION = "/viewAuction.jsp";

    public BidServlet() {
        super();
        auctionDao = new AuctionDAO();
        itemDao = new ItemDAO();
        userDao = new UserDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Bid bid = new Bid();
        int auctionId = Integer.parseInt(request.getParameter("auctionId"));
        String now = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
        bid.setAuctionId(auctionId);
        bid.setItemId(Integer.parseInt(request.getParameter("itemId")));
        bid.setBidderId(Integer.parseInt(request.getParameter("bidderId")));
        bid.setCurrentMaxBid(Integer.parseInt(request.getParameter("bid")));
        bid.setBidPlaced(Integer.parseInt(request.getParameter("bid")));
        bid.setBidTime(now);
        Auction auction = auctionDao.getAuctionById(auctionId);
        
        Item item = itemDao.getItemById(bid.getItemId());
        Post post = auctionDao.getPostById(auctionId);
        request.setAttribute("auction", auction);
        request.setAttribute("item", item);
        request.setAttribute("post",post);
        request.setAttribute("seller",userDao.getUserById(post.getSellerId()));
        
        
        if(bid.getBidPlaced() > auction.getCurrentBid()){
            auction.setCurrentBid(bid.getBidPlaced());
            auctionDao.addBid(bid);
        }
        if(auction.getCurrentBid() >= auction.getReservePrice()){
            auction.setFinished(true);
            auctionDao.setFinished(auction.getAuctionId());
            auctionDao.setAuctionWinner(auction.getAuctionId(),bid.getBidderId());
            auctionDao.setCurrentBid(auction.getCurrentBid(),auction.getAuctionId());
            itemDao.incrementCopiesSold(bid.getItemId());
            itemDao.decrementAmountInStock(bid.getItemId());
        }
        request.setAttribute("bids", auctionDao.getBidsByAuctionId(auctionId));
        RequestDispatcher view = request.getRequestDispatcher(VIEW_AUCTION);
        view.forward(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
