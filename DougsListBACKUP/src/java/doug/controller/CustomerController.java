package doug.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import doug.dao.CustomerDAO;
import doug.dao.ItemDAO;
import doug.model.Customer;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ShawnCruz
 */
public class CustomerController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/addCustomer.jsp";
    private static String LIST_CUSTOMER = "/listCustomer.jsp";
    private static String BEST_SELLERS = "/bestSellers.jsp";
    private static String ITEMS_BY_SELLER = "sales/itemsBySeller.jsp";
    private static String ITEMS_BY_TYPE = "sales/itemsByType.jsp";
    private static String ITEMS_BY_KEYWORD = "sales/itemsByKeyword.jsp";
    private static String ITEM_SUGGESTIONS = "/itemSuggestions.jsp";
    private CustomerDAO dao;
    private ItemDAO itemDAO;

    public CustomerController() {
        super();
        dao = new CustomerDAO();
        itemDAO = new ItemDAO();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String forward="";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("delete")){
            
            int customerId = Integer.parseInt(request.getParameter("customerId"));
            dao.deleteCustomer(customerId);
            forward = LIST_CUSTOMER;
            request.setAttribute("customers", dao.getAllCustomers());
            
        } else if (action.equalsIgnoreCase("edit")){
            
            forward = INSERT_OR_EDIT;
            int customerId = Integer.parseInt(request.getParameter("customerId"));
            Customer customer = dao.getCustomerById(customerId );
            request.setAttribute("customer", customer);
            
        } else if (action.equalsIgnoreCase("listCustomers")){
            
            forward = LIST_CUSTOMER;
            request.setAttribute("customers", dao.getAllCustomers());
            request.getSession().setAttribute("customers", dao.getAllCustomers());
            
        } 
        /* Although best seller code exists in the manager servlet, it is 
           repeated here to prevent unauthorized access to manager functions
           by users */
        else if (action.equalsIgnoreCase("bestSellers")){
            
            forward = BEST_SELLERS;
            request.setAttribute("bestSellers", itemDAO.getBestSellers());
            
        } 
        else if (action.equalsIgnoreCase("itemsBySeller")){    
            forward = ITEMS_BY_SELLER;
            int customerId = Integer.parseInt(request.getParameter("sellerId"));
            Customer customer = dao.getCustomerById(customerId );
            request.setAttribute("customer", customer);
            request.setAttribute("itemsBySeller", itemDAO.getItemsBySeller(customerId));
            
        } 
        else if (action.equalsIgnoreCase("itemsByType")){    
            forward = ITEMS_BY_TYPE;
            String itemType = request.getParameter("itemType");
            request.setAttribute("itemsByType", itemDAO.getItemsByType(itemType));
            
        } 
        else if (action.equalsIgnoreCase("itemsByKeyword")){    
            forward = ITEMS_BY_KEYWORD;
            String keyword = request.getParameter("keyword");
            request.setAttribute("keyword",keyword);
            request.setAttribute("itemsByKeyword", itemDAO.getItemsByKeyword(keyword));
            
        }
        else if (action.equalsIgnoreCase("itemSuggestions")){
            
            forward = ITEM_SUGGESTIONS;
            int customerId = Integer.parseInt(request.getParameter("customerId"));
            Customer customer = dao.getCustomerById(customerId);
            request.setAttribute("customer", customer);
            request.setAttribute("itemSuggestions", itemDAO.getSuggestedItem(customerId));
            
        } 
        else {
            forward = INSERT_OR_EDIT;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Customer customer = new Customer();
        customer.setPassword(request.getParameter("password"));
        customer.setLastName(request.getParameter("lastName"));
        customer.setFirstName(request.getParameter("firstName"));
        customer.setAddress(request.getParameter("address"));
        customer.setCity(request.getParameter("city"));
        customer.setState(request.getParameter("state"));
        customer.setZipcode(Integer.parseInt(request.getParameter("zipcode")));
        customer.setTelephone(request.getParameter("telephone"));
        customer.setEmail(request.getParameter("email"));
        customer.setRating(request.getParameter("rating"));
        customer.setCreditCard(request.getParameter("creditCardNum"));

        String customerId = request.getParameter("customerId");

        if(customerId == null || customerId.isEmpty()) {
            dao.addCustomer(customer);
        }
        else {
            customer.setCustomerId(Integer.parseInt(customerId));
            dao.updateCustomer(customer);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_CUSTOMER);
        request.setAttribute("customers", dao.getAllCustomers());
        view.forward(request, response);
    }

}
