package doug.model;

/**
 * CREATE TABLE `MailingEntry` (
       `Monitor` INTEGER NOT NULL,
       `Customer` INTEGER NOT NULL,
       `Email` VARCHAR(30),
               PRIMARY KEY(`Monitor`,`Customer`),
               FOREIGN KEY(`Monitor`) REFERENCES `Employee` (`EmployeeID`),
               FOREIGN KEY(`Customer`) REFERENCES `Customer` (`CustomerID`)
                       ON DELETE NO ACTION
                       ON UPDATE CASCADE
 );
 * 
 * @author ShawnCruz
 */
public class MailingEntry {
    
    private int monitor;
    private int customer;
    private String email;

    public int getMonitor() {
        return monitor;
    }

    public void setMonitor(int monitor) {
        this.monitor = monitor;
    }

    public int getCustomer() {
        return customer;
    }

    public void setCustomer(int customer) {
        this.customer = customer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
