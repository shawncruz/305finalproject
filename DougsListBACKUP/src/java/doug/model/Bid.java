package doug.model;

/**
 *
 * CREATE TABLE `Bid` (
 *      `BidderID`		INTEGER NOT NULL,
 *      `BidPlaced`		NUMERIC(15,2),
 *      `CurrentMaxBid`		NUMERIC(15,2),
 *      `AuctionID`		INTEGER(9),
 *      `ItemID`		INTEGER(9),
 *      `BidTime`		DATETIME,
 *          PRIMARY KEY (`BidderID`, `ItemID`, `BidTime`),
 *          FOREIGN KEY(`BidderID`) REFERENCES `Customer` (`CustomerID`)
 *		ON DELETE NO ACTION
 *		ON UPDATE CASCADE,
 *          FOREIGN KEY(`ItemID`) REFERENCES `Item` (`ItemID`)
 *		ON DELETE NO ACTION
 *		ON UPDATE CASCADE,
 *          FOREIGN KEY(`AuctionID`) REFERENCES `Auction` (`AuctionID`)
 *		ON DELETE NO ACTION
 *		ON UPDATE CASCADE );
 * 
 * @author ShawnCruz
 */
public class Bid {
    
    private int bidderId;
    private int bidPlaced;
    private int currentMaxBid;
    private int auctionId;
    private int itemId;
    private String bidTime;

    public int getBidderId() {
        return bidderId;
    }

    public void setBidderId(int bidderId) {
        this.bidderId = bidderId;
    }

    public int getBidPlaced() {
        return bidPlaced;
    }

    public void setBidPlaced(int bidPlaced) {
        this.bidPlaced = bidPlaced;
    }

    public int getCurrentMaxBid() {
        return currentMaxBid;
    }

    public void setCurrentMaxBid(int currentMaxBid) {
        this.currentMaxBid = currentMaxBid;
    }

    public int getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(int auctionId) {
        this.auctionId = auctionId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getBidTime() {
        return bidTime;
    }

    public void setBidTime(String bidTime) {
        this.bidTime = bidTime;
    }
}
