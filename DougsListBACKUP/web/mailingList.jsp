<%-- 
    Document   : mailingList
    Created on : Nov 21, 2015, 2:49:46 AM
    Author     : ShawnCruz
--%>

<%@page import="java.util.List"%>
<%@page import="doug.model.Employee"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Mailing List</title>  
    </head>  
    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="inner-container">
                <h5><c:out value="${currentUser.firstName}"/>'s Customer (e)Mailing List</h5>
                <table border=1>
                    <thead>
                        <tr>
                            <th>Monitor Id</th>
                            <th>Customer Id</th>
                            <th>Customer Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${mailingList}" var="entry">
                            <tr>        
                                <td><c:out value="${entry.monitor}"/></td>
                                <td><c:out value="${entry.customer}"/></td>
                                <td><c:out value="${entry.email}"/></td>
                                <td><a href="RepresentativeLogicServlet?action=removeRecipient&customerId=<c:out value="${entry.customer}"/>">Remove</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="submit-button">
                    <form method="POST" action='RepresentativeLogicServlet' name="frmMailingList">    
                        <p>Enter customer's ID:</p>
                        <input type="text" name="customerId"/>
                        <input type="submit" value="Submit"/>
                    </form>
                </div>
                </body>
            </div>
        </div>
</html>
