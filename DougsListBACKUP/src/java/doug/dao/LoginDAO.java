package doug.dao;

import doug.model.Customer;
import doug.model.Employee;
import doug.model.User;
import doug.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ShawnCruz
 */
public class LoginDAO {
    
    private Connection connection;

    public LoginDAO() {
        connection = DatabaseUtility.getConnection();
    }
    
    public User getUser(String username) { 
        User user = new User();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM User WHERE Email=?");
            preparedStatement.setString(1, username); // username = Email attribute of User table
            ResultSet rs = preparedStatement.executeQuery();
            
            if (rs.next()) {
                user.setUserId(rs.getInt("UserID"));
                user.setPassword(rs.getString("Password"));
                user.setLastName(rs.getString("LastName"));
                user.setFirstName(rs.getString("FirstName"));
                user.setAddress(rs.getString("Address"));
                user.setCity(rs.getString("City"));
                user.setState(rs.getString("State"));
                user.setZipcode(rs.getInt("ZipCode"));
                user.setTelephone(rs.getString("Telephone"));
                user.setEmail(rs.getString("Email"));
            }
            
            user = getUserType(user);
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }
    
    private User getUserType(User user) throws SQLException {
        
        User resultantUser = new User();
        
        /**
         * Determine faulty output by testing user id against all Customer id(s) in table.
         *  IF valid customer: testCustomer will return a customer object with a valid id
         *  ELSE invalid customer: testCustomer will return a customer object with an invalid id = 0
         */
        Customer customer = testCustomer(user); 
        if(customer.getCustomerId() != 0)
            resultantUser = customer;
        
        /**
         * Determine faulty output by testing user id against all Employee id(s) in table.
         *  IF valid employee: testEmployee will return a employee object with a valid id
         *  ELSE invalid employee: testEmployee will return a employee object with an invalid id = 0
         */
        Employee employee = testEmployee(user); // Determine if user is a Customer
        if(employee.getEmployeeId() != 0) 
            resultantUser = employee;
        
        // Return result of tests
        return resultantUser;
    }
    
    private Customer testCustomer(User user) throws SQLException {
        int userId = user.getUserId();
        // Query to see if userId corresponds to a Customer
        PreparedStatement p1 = connection.prepareStatement("SELECT * FROM Customer WHERE CustomerID=?");
        p1.setInt(1, userId); // userId = UserId attribute of User table
        ResultSet r1 = p1.executeQuery();
        
        Customer customer = createCustomer(r1);
        customer.setUserInfo(user);
        p1.close();
        r1.close();
        
        return customer;
    }
    
    private Employee testEmployee(User user) throws SQLException {
        int userId = user.getUserId();
          // Query to see if email corresponds to a Employee
        PreparedStatement p2 = connection.prepareStatement("SELECT * FROM Employee WHERE EmployeeID=?");
        p2.setInt(1, userId); // userId = UserId attribute of User table
        ResultSet r2 = p2.executeQuery();
        
        Employee employee = createEmployee(r2);
        employee.setUserInfo(user);
        p2.close();
        r2.close();
   
        return employee;
    }
    
    private Customer createCustomer(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        if (rs.next()) {
                customer.setCustomerId(rs.getInt("CustomerID"));
                customer.setRating(rs.getString("Rating"));
                customer.setCreditCard(rs.getString("CreditCardNum"));
            }
        return customer;
    }
    
    private Employee createEmployee(ResultSet rs) throws SQLException {
        Employee employee = new Employee();
        if (rs.next()) {
                employee.setEmployeeId(rs.getInt("EmployeeID"));
                employee.setStartDate(rs.getString("StartDate"));
                employee.setHourlyRate(rs.getString("HourlyRate"));
                employee.setLvl(rs.getInt("Lvl"));
            }
        return employee;
    }
    
    public boolean validate(String username, String password) {
        
        boolean status = false; 
        ResultSet resultSet = null;
        
        try {
             
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM User WHERE Email=? AND Password=?");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            status = resultSet.next();
            
            // Release all unused statements, sets, and connections
            //DatabaseUtility.close(); // or connection.close(); ???
            //preparedStatement.close();
            //resultSet.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }
}
