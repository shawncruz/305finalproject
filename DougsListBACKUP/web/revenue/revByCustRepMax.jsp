<%-- 
    Document   : revByCustRepMax
    Created on : Nov 20, 2015, 7:19:57 PM
    Author     : ShawnCruz
--%>

<%@page import="java.util.List"%>
<%@page import="doug.model.Employee"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
     <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Top-Performing Customer Representative</title>  
    </head> 
    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="inner-container">
                <h4>Top-Performing Customer Representative</h4>
                <table border=1 width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Revenue</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><c:out value="${custRepMax.name}"/></td>
                            <td>$<c:out value="${custRepMax.revenue}"/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
