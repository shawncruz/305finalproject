package doug.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import doug.dao.CustomerDAO;
import doug.dao.ItemDAO;
import doug.dao.MailingListDAO;
import doug.model.Customer;
import doug.model.Employee;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ShawnCruz
 */
public class RepresentativeLogicServlet extends HttpServlet {

    private static String MAILING_LIST = "/mailingList.jsp";
    private static String ITEM_SUGGESTIONS = "/itemSuggestions.jsp";
    private static String ERROR = "/error.jsp";
    
    private MailingListDAO mailingListDAO;
    private CustomerDAO customerDAO;
    private ItemDAO itemDAO;
    
    public RepresentativeLogicServlet() {
        super();
        mailingListDAO = new MailingListDAO();
        customerDAO = new CustomerDAO();
        itemDAO = new ItemDAO();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String forward="";
        String action = request.getParameter("action");
        Employee employee = (Employee)request.getSession().getAttribute("currentUser");
        int employeeId = employee.getEmployeeId();

        if (action.equalsIgnoreCase("mailingList")){
            
            forward = MAILING_LIST;  
            request.setAttribute("mailingList", mailingListDAO.getMailingList(employeeId));
            
        } else if (action.equalsIgnoreCase("removeRecipient")){
            
            forward = MAILING_LIST;
            int customerId = Integer.parseInt(request.getParameter("customerId"));
            mailingListDAO.deleteRecipient(customerId);
            request.setAttribute("mailingList", mailingListDAO.getMailingList(employeeId));
            
        }  else if (action.equalsIgnoreCase("itemSuggestions")){
            
            forward = ITEM_SUGGESTIONS;
            int customerId = Integer.parseInt(request.getParameter("customerId"));
            Customer customer = customerDAO.getCustomerById(customerId);
            request.setAttribute("customer", customer);
            request.setAttribute("itemSuggestions", itemDAO.getSuggestedItem(customerId));
            
        } else {
            forward = ERROR;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int customerId = Integer.parseInt(request.getParameter("customerId"));
        Employee employee = (Employee)request.getSession().getAttribute("currentUser");
        int employeeId = employee.getEmployeeId();

        mailingListDAO.addCustomerToList(customerId, employeeId);

        RequestDispatcher view = request.getRequestDispatcher(MAILING_LIST);
        request.setAttribute("mailingList", mailingListDAO.getMailingList(employeeId));
        view.forward(request, response);
    }



}
