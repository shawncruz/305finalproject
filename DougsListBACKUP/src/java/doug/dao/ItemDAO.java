package doug.dao;

import doug.model.Item;
import doug.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ShawnCruz
 */
public class ItemDAO {

    private Connection connection;

    public ItemDAO() {
        connection = DatabaseUtility.getConnection();
    }

    public void addItem(Item item) {
        int itemId = 0;
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO Item(Description, ItemName, Year, "
                            + "ItemType, AmountInStock, CopiesSold)"
                            + " VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, item.getDescription());
            preparedStatement.setString(2, item.getItemName());
            preparedStatement.setInt(3, item.getYear());
            preparedStatement.setString(4, item.getItemType());
            preparedStatement.setInt(5, 1);
            preparedStatement.setInt(6, 0);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Item> getSuggestedItem(int customerId) {
        List<Item> items = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("    SELECT DISTINCT i.ItemId, Description, ItemName, Year, i.ItemType, AmountInStock, CopiesSold\n"
                            + "    FROM Item i, Auction a,\n"
                            + "    (  \n"
                            + "    SELECT Item.ItemType, COUNT(Item.ItemType) AS num_bids\n"
                            + "    FROM Bid, Item\n"
                            + "    WHERE Item.ItemID=Bid.ItemID AND Bid.BidderID = 10000000\n"
                            + "    GROUP BY Item.ItemType\n"
                            + "    ORDER BY num_bids DESC, BID.BidTime DESC) AS BidItem,\n"
                            + "    (\n"
                            + "    SELECT ItemID\n"
                            + "    FROM Bid\n"
                            + "    WHERE BidderID = 10000000\n"
                            + "    ) AS ItemsToExclude\n"
                            + "    WHERE BidItem.ItemType=i.ItemType\n"
                            + "    AND a.ItemID=i.ItemID\n"
                            + "    AND a.finished=0\n"
                            + "    AND i.ItemID != ItemsToExclude.ItemID;");
            preparedStatement.setInt(1, customerId);
            preparedStatement.setInt(2, customerId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Item item = new Item();
                item.setItemId(rs.getInt("ItemID"));
                item.setDescription(rs.getString("Description"));
                item.setItemName(rs.getString("ItemName"));
                item.setYear(rs.getInt("Year"));
                item.setItemType(rs.getString("ItemType"));
                item.setAmountInStock(rs.getInt("AmountInStock"));
                item.setCopiesSold(rs.getInt("CopiesSold"));
                items.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return items;
    }
    
        public void incrementCopiesSold(int itemId) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Item I SET I.CopiesSold = I.CopiesSold+1 WHERE I.ItemId = ?;");

            preparedStatement.setInt(1, itemId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void incrementAmountInStock(int itemId) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Item I SET I.AmountInStock = I.AmountInStock+1 WHERE I.ItemId = ?;");

            preparedStatement.setInt(1, itemId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
        public void decrementAmountInStock(int itemId) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Item I SET I.AmountInStock = I.AmountInStock-1 WHERE I.ItemId = ?;");

            preparedStatement.setInt(1, itemId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Item> getAllItems() {
        List<Item> items = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM Item");
            while (rs.next()) {
                Item item = new Item();
                item.setItemId(rs.getInt("ItemID"));
                item.setDescription(rs.getString("Description"));
                item.setItemName(rs.getString("ItemName"));
                item.setYear(rs.getInt("Year"));
                item.setItemType(rs.getString("ItemType"));
                item.setAmountInStock(rs.getInt("AmountInStock"));
                item.setCopiesSold(rs.getInt("CopiesSold"));
                items.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return items;
    }

    public List<Item> getBestSellers() {
        List<Item> items = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            /* ResultSet rs = statement.executeQuery("SELECT Item.ItemName AS ItemName, COUNT(Item.ItemName) AS ItemCount\n" +
             "FROM Item, Auction, Post\n" +
             "WHERE Item.ItemID = Auction.ItemID AND Auction.AuctionID = Post.AuctionID AND\n" +
             "Auction.Finished = 1\n" +
             "GROUP BY Item.ItemID\n" +
             "ORDER BY COUNT(*) DESC\n" +
             "LIMIT 3;");*/
            ResultSet rs = statement.executeQuery("SELECT Item.ItemName,Item.CopiesSold FROM auction.item\n"
                    + "ORDER BY CopiesSold DESC\n"
                    + "LIMIT 3;");

            while (rs.next()) {
                Item item = new Item();
                item.setItemName(rs.getString("ItemName"));
                item.setCopiesSold(rs.getInt("CopiesSold"));
                items.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return items;
    }

    public List<Item> getItemsBySeller(int id) {
        List<Item> items = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("SELECT User.FirstName, User.LastName, Item.ItemName, Auction.AuctionID\n"
                    + "FROM Customer, User, Post, Auction, Item\n"
                    + "WHERE Customer.CustomerID = " + id + " AND Customer.CustomerID = User.UserID AND Customer.CustomerID = Post.SellerID AND\n"
                    + "Auction.AuctionID = Post.AuctionID AND Auction.Finished = FALSE AND Item.ItemID = Auction.ItemID;");

            while (rs.next()) {
                Item item = new Item();
                item.setItemName(rs.getString("ItemName"));
                items.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return items;
    }

    public List<Item> getItemsByType(String itemType) {
        List<Item> items = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("SELECT  Item.ItemName, Item.Description, Item.Year, Auction.CurrentWinner, Auction.CurrentBid\n"
                    + "FROM Item, Auction\n"
                    + "WHERE Item.ItemType = '" + itemType + "' AND "
                    + "Item.ItemID = Auction.ItemId AND Auction.Finished = FALSE;");

            while (rs.next()) {
                Item item = new Item();
                item.setItemName(rs.getString("ItemName"));
                item.setDescription(rs.getString("Description"));
                item.setYear(rs.getInt("Year"));
                items.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return items;
    }

    public List<Item> getItemsByKeyword(String keyword) {
        List<Item> items = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("SELECT Item.ItemName, Item.Description, Item.Year\n"
                    + "FROM Item, Auction\n"
                    + "WHERE Item.ItemName LIKE '%" + keyword + "%' AND Item.ItemID = Auction.ItemID AND Auction.Finished = FALSE;");

            while (rs.next()) {
                Item item = new Item();
                item.setItemName(rs.getString("ItemName"));
                item.setDescription(rs.getString("Description"));
                item.setYear(rs.getInt("Year"));
                items.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return items;
    }

    public Item getItemByName(String name) {
        Item item = new Item();
        try {
            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("SELECT * FROM Item WHERE Item.ItemName = '" + name + "';");

            while (rs.next()) {
                item.setItemName(rs.getString("ItemName"));
                item.setItemId(rs.getInt("ItemID"));
                item.setDescription(rs.getString("Description"));
                item.setYear(rs.getInt("Year"));
                item.setItemType(rs.getString("ItemType"));
                item.setCopiesSold(rs.getInt("CopiesSold"));
                item.setAmountInStock(rs.getInt("AmountInStock"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return item;
    }

    public Item getItemById(int itemId) {
        Item item = new Item();
        try {
            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("SELECT * FROM Item WHERE Item.ItemId = " + itemId + ";");

            while (rs.next()) {
                item.setItemName(rs.getString("ItemName"));
                item.setItemId(rs.getInt("ItemID"));
                item.setDescription(rs.getString("Description"));
                item.setYear(rs.getInt("Year"));
                item.setItemType(rs.getString("ItemType"));
                item.setCopiesSold(rs.getInt("CopiesSold"));
                item.setAmountInStock(rs.getInt("AmountInStock"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return item;
    }
}
