/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package doug.controller;

import doug.dao.AuctionDAO;
import doug.dao.ItemDAO;
import doug.dao.SalesDAO;
import doug.model.Item;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet handles addition/modification of items in the database
 * @author Lauren
 */
public class ItemServlet extends HttpServlet {
    
    private static String CREATE_AUCTION = "/createAuction.jsp";
    private static String CREATE_ITEM = "/createItem.jsp";
    
    private ItemDAO itemDao;
    
    public ItemServlet() {
        super();
        itemDao = new ItemDAO();
       }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                Item item = new Item();
                item.setItemName(request.getParameter("itemName"));
                item.setDescription(request.getParameter("itemDescription"));
                item.setYear(Integer.parseInt(request.getParameter("itemYear")));
                item.setItemType(request.getParameter("itemType"));
                itemDao.addItem(item);
                
                RequestDispatcher view = request.getRequestDispatcher(CREATE_AUCTION);
                view.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
