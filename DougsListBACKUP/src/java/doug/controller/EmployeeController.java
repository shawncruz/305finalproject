package doug.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import doug.dao.EmployeeDAO;
import doug.model.Employee;
import doug.model.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ShawnCruz
 */
public class EmployeeController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/addEmployee.jsp";
    private static String LIST_EMPLOYEE = "/listEmployee.jsp";
    private EmployeeDAO dao;

    public EmployeeController() {
        super();
        dao = new EmployeeDAO();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("delete")){
            
            int employeeId = Integer.parseInt(request.getParameter("employeeId"));
            dao.deleteEmployee(employeeId);
            forward = LIST_EMPLOYEE;
            request.setAttribute("employees", dao.getAllEmployees());
            
        } else if (action.equalsIgnoreCase("edit")){
            
            forward = INSERT_OR_EDIT;
            int userId = Integer.parseInt(request.getParameter("employeeId"));
            Employee employee = dao.getEmployeeById(userId);
            request.setAttribute("employee", employee);
            
        } else if (action.equalsIgnoreCase("listEmployees")){
            
            forward = LIST_EMPLOYEE;
            request.setAttribute("employees", dao.getAllEmployees());
            request.getSession().setAttribute("employees", dao.getAllEmployees());
            
        } else {
            forward = INSERT_OR_EDIT;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Employee employee = new Employee();
        employee.setPassword(request.getParameter("password"));
        employee.setLastName(request.getParameter("lastName"));
        employee.setFirstName(request.getParameter("firstName"));
        employee.setAddress(request.getParameter("address"));
        employee.setCity(request.getParameter("city"));
        employee.setState(request.getParameter("state"));
        employee.setZipcode(Integer.parseInt(request.getParameter("zipcode")));
        employee.setTelephone(request.getParameter("telephone"));
        employee.setEmail(request.getParameter("email"));
        employee.setStartDate(request.getParameter("startDate"));
        employee.setHourlyRate(request.getParameter("hourlyRate"));
        employee.setLvl(Integer.parseInt(request.getParameter("lvl")));

        String employeeId = request.getParameter("employeeId");

        if(employeeId == null || employeeId.isEmpty()) {
            dao.addEmployee(employee);
        }
        else {
            employee.setEmployeeId(Integer.parseInt(employeeId));
            dao.updateEmployee(employee);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_EMPLOYEE);
        request.setAttribute("employees", dao.getAllEmployees());
        view.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
