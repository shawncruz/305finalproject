package doug.dao;

import doug.controller.AuctionServlet;
import doug.model.Auction;
import doug.model.Bid;
import doug.model.Post;
import doug.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ShawnCruz
 */
public class AuctionDAO {

    private Connection connection;
    private ItemDAO itemDao;

    public AuctionDAO() {
        connection = DatabaseUtility.getConnection();
    }

    public Auction addAuction(Auction auction, Post post) {
        int auctionId = 0;
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO Auction(ItemID, Monitor, CurrentWinner, "
                            + "BidIncrement, CurrentBid, Finished, ReservePrice)"
                            + " VALUES (?, ?, ?, ?, ?, ?, ?)",
                            Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, auction.getItemId());
            preparedStatement.setInt(2, 10000000);
            preparedStatement.setString(3, null);
            preparedStatement.setDouble(4, auction.getBidIncrement());
            preparedStatement.setDouble(5, 0);
            preparedStatement.setBoolean(6, false);
            preparedStatement.setDouble(7, auction.getReservePrice());
            preparedStatement.executeUpdate();
            //Retrieve auto-incremented auction id
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                auctionId = rs.getInt(1);
                auction.setAuctionId(auctionId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO Post(ExpireDate,PostDate,SellerID,AuctionID) VALUES (?, ?, ?, ?)");
            preparedStatement.setString(1, post.getExpireDate());
            preparedStatement.setString(2, post.getPostDate());
            preparedStatement.setInt(3, post.getSellerId());
            preparedStatement.setInt(4, auctionId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return auction;
    }

    public void addBid(Bid bid) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO Bid(BidderID, BidPlaced, CurrentMaxBid, "
                            + "AuctionID, ItemID, BidTime)"
                            + " VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement.setInt(1, bid.getBidderId());
            preparedStatement.setDouble(2, bid.getBidPlaced());
            preparedStatement.setDouble(3, bid.getCurrentMaxBid());
            preparedStatement.setInt(4, bid.getAuctionId());
            preparedStatement.setInt(5, bid.getItemId());
            preparedStatement.setString(6, bid.getBidTime());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setFinished(int auctionId) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Auction A SET A.Finished = TRUE WHERE A.AuctionID = ?");

            preparedStatement.setInt(1, auctionId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets all ongoing auctions.(Only from the auction table) Will need to
     * account for posts and bids but can easily be extended
     *
     * @return
     */
    public List<Auction> getOngoingAuctions() {
        List<Auction> auctions = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM Auction");
            while (rs.next()) {
                Auction auction = new Auction();
                auction.setAuctionId(rs.getInt("AuctionID"));
                auction.setItemId(rs.getInt("ItemID"));
                auction.setMonitor(rs.getInt("Monitor"));
                auction.setCurrentWinner(rs.getInt("CurrentWinner"));
                auction.setBidIncrement(rs.getDouble("BidIncrement"));
                auction.setCurrentBid(rs.getInt("CurrentBid"));
                auction.setFinished(rs.getBoolean("Finished"));
                auction.setReservePrice(rs.getDouble("ReservePrice"));
                auctions.add(auction);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return auctions;
    }
    
        public Auction getAuctionById(int auctionId) {
        Auction auction = new Auction();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM Auction WHERE Auction.AuctionId = "+auctionId);
            while (rs.next()) {
                auction.setAuctionId(rs.getInt("AuctionID"));
                auction.setItemId(rs.getInt("ItemID"));
                auction.setMonitor(rs.getInt("Monitor"));
                auction.setCurrentWinner(rs.getInt("CurrentWinner"));
                auction.setBidIncrement(rs.getDouble("BidIncrement"));
                auction.setCurrentBid(rs.getInt("CurrentBid"));
                auction.setFinished(rs.getBoolean("Finished"));
                auction.setReservePrice(rs.getDouble("ReservePrice"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return auction;
    }
        
     public Post getPostById(int auctionId) {
        Post post = new Post();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM Post WHERE Post.AuctionId = "+auctionId);
            while (rs.next()) {
                post.setExpireDate(rs.getString("ExpireDate"));
                post.setPostDate(rs.getString("PostDate"));
                post.setSellerId(rs.getInt("SellerId"));
                post.setAuctionId(rs.getInt("AuctionID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return post;
    }


    public List<Bid> getBidsByAuctionId(int auctionId) {

        List<Bid> bids = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("    SELECT Bid.*\n"
                    + "	FROM  Auction, Bid\n"
                    + "	WHERE Auction.AuctionID = "+auctionId+" AND Bid.AuctionId = "+auctionId+";");
            while (rs.next()) {
                Bid bid = new Bid();
                 bid.setBidderId(rs.getInt("BidderId"));
                bid.setAuctionId(rs.getInt("AuctionID"));
                bid.setBidPlaced(rs.getInt("BidPlaced"));
                bid.setCurrentMaxBid(rs.getInt("CurrentMaxBid"));
                bid.setBidTime(rs.getString("BidTime"));
                bids.add(bid);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bids;
    }
    
    public void setAuctionWinner(int auctionId,int winnerId){
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Auction A SET A.CurrentWinner = ? WHERE A.AuctionID = ?");
            
            preparedStatement.setInt(1, winnerId);
            preparedStatement.setInt(2, auctionId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
        public void setCurrentBid(double bid,int auctionId){
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Auction A SET A.CurrentBid = ? WHERE A.AuctionID = ?");
            
            preparedStatement.setDouble(1, bid);
            preparedStatement.setInt(2, auctionId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
}
