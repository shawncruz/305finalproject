package doug.model;

/**
 *
 * CREATE TABLE `Post` ( 
 *  `ExpireDate`		DATETIME,
 *  `PostDate`			DATETIME,
 *  `SellerID`			INTEGER NOT NULL,
 *  `AuctionID`			INTEGER(9),
 *      PRIMARY KEY (`SellerID`, `AuctionID`),
 *      FOREIGN KEY (`SellerID`) REFERENCES `Customer` (`CustomerID`)
 *		ON DELETE NO ACTION
 *		ON UPDATE CASCADE,
 *      FOREIGN KEY (`AuctionID`) REFERENCES `Auction` (`AuctionID`)
 *		ON DELETE NO ACTION
 *		ON UPDATE CASCADE ,
 *        CHECK (`AuctionID` > 0 AND `AuctionID` < 1000000000)	);
 * 
 * @author ShawnCruz
 */
public class Post {
    
    private String expireDate;
    private String postDate;
    private int sellerId;
    private int auctionId;

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public int getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(int auctionId) {
        this.auctionId = auctionId;
    }
}
