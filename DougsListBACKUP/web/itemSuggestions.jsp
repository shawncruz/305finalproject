<%-- 
    Document   : itemSuggestions
    Created on : Nov 21, 2015, 6:41:00 PM
    Author     : ShawnCruz
--%>

<%@page import="java.util.List"%>
<%@page import="doug.model.Employee"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Item Suggestions</title>  
    </head>  
    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>

        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>

        <div class ='container'>
            <h3><c:out value="${customer.firstName}"/>'s Item Suggestion List</h3>
            <hr>
            <table width="100%" border=1>
                <thead>
                    <tr>
                        <th>Item Name</th>
                        <th>Description</th>
                        <th>Year</th>
                        <th>Item Type</th>
                        <th>In Stock</th>
                        <th>Copies Sold</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${itemSuggestions}" var="item">
                        <tr>        
                            <td><c:out value="${item.itemName}"/></td>
                            <td><c:out value="${item.description}"/></td>
                            <td><c:out value="${item.year}"/></td>
                            <td><c:out value="${item.itemType}"/></td>
                            <td><c:out value="${item.amountInStock}"/></td>
                            <td><c:out value="${item.copiesSold}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
