package doug.model;

/**
 *  CREATE TABLE `Employee` (
 *      `EmployeeID`	INTEGER NOT NULL,
 *      `StartDate`		DATETIME,
 *      `HourlyRate`	NUMERIC(15,2),
 *      `Lvl`			INTEGER,
 *      PRIMARY KEY (`EmployeeID`),
 *      FOREIGN KEY (`EmployeeID`) REFERENCES `User` (`UserID`)
 *		ON DELETE NO ACTION
 *		ON UPDATE CASCADE);
 * 
 * @author ShawnCruz
 */
public class Employee extends User {
    
    private int employeeId;
    private String startDate;
    private String hourlyRate;
    private int lvl;

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
        this.userId = employeeId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(String hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }
    
    public void setUserInfo(User userInfo) {
        this.userId = userInfo.getUserId();
        //this.employeeId = userId;
        this.password = userInfo.getPassword();
        this.lastName = userInfo.getLastName();
        this.firstName = userInfo.getFirstName();
        this.address = userInfo.getAddress();
        this.city = userInfo.getCity();
        this.state = userInfo.getState();
        this.zipcode = userInfo.getZipcode();
        this.telephone = userInfo.getTelephone();
        this.email = userInfo.getEmail();
    }
    
    @Override
    public String toString() {
        return "Employee [employeeid=" + employeeId +  "]";
    }
}
