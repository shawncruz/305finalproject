<%-- 
    Customer Page: 
        This is where customers will be brought upon valid login
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>  
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Welcome <c:out value="${currentUser.firstName}"/></title>  
    </head>  
    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>

        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>

        <div class ='container'>
        <h3><c:out value="${currentUser.firstName}"/>'s Account</h3>  
        <hr>
        <p class="user-info">
            Name: ${currentUser.firstName} ${currentUser.lastName} <br>
            Rating: ${currentUser.rating} <br>
            Address: ${currentUser.address}, ${currentUser.city}, ${currentUser.state} ${currentUser.zipcode}<br>
            Telephone: ${currentUser.telephone}<br>
            Email: ${currentUser.email}
        </p>
        <br><br>
        <h6> Auction Tools </h6>
        <hr>
        <p>
        <a href ="createAuction.jsp">Create Auction</a><br>
        <a href="AuctionServlet?action=listAuctionsCustomer">List Auctions</a><br>
        <a href="CustomerController?action=itemSuggestions&customerId=${currentUser.userId}">My Item Suggestions</a><br>
        </p>
        <h6>Find Items</h6>
        <hr>
        <form method="GET" action="CustomerController">
            <p><input style='display:none' name='action' type='text' value='bestSellers'>
            <input type="submit" value="Best Sellers List"></p>
        </form>
        <form method="GET" action="CustomerController">
            <p><input style='display:none' name='action' type='text' value='itemsBySeller'>
                Produce List of Items by Seller (enter seller id):<br>
            <input type="text" name="sellerId">
            <input type="submit" value="Submit"></p>
        </form>
        <form method="GET" action="CustomerController">
            <p><input style='display:none' name='action' type='text' value='itemsByType'>
                Produce List of Items by Type (enter item type):<br>
            <input type="text" name="itemType">
            <input type="submit" value="Submit"></p>
        </form>
        <form method="GET" action="CustomerController">
           <p> <input style='display:none' name='action' type='text' value='itemsByKeyword'>
            Produce List of Items by Keyword:<br>
            <input type="text" name="keyword">
            <input type="submit" value="Submit"></p>
        </form>
        </div>
    </body>  
</html>  
