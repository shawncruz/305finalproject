<%-- 
    Error Page:
        User will be taken to this page upon encountering an unknown error.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error Page</title>
    </head>
    <body>
        <h1 style="color:red">Whoops!</h1>
        <p>You 
        <p style="padding-left:5em">have</p>  
        <p style="padding-left:10em">encountered</p>  
        <p style="padding-left:15em">some</p>
        <p style="padding-left:20em">errors.</p>
        Check logs for more info.</p>
    </body>
</html>
