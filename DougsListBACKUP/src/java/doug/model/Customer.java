package doug.model;

/**
 * 
 * CREATE TABLE `Customer` (
 *  `CustomerID`	INTEGER NOT NULL,
 *  `Rating`		CHAR,
 *  `CreditCardNum`	VARCHAR(16),
 *  PRIMARY KEY (`CustomerID`),
 *  FOREIGN KEY (`CustomerID`) REFERENCES `User` (`UserID`)
 *		ON DELETE NO ACTION
 *		ON UPDATE CASCADE);
 */

/**
 *
 * @author ShawnCruz
 */
public class Customer extends User {
    
    private int customerId;
    private String rating;
    private String creditCard;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
        this.userId = customerId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public void setUserInfo(User userInfo) {
        this.userId = userInfo.getUserId();
        this.password = userInfo.getPassword();
        this.lastName = userInfo.getLastName();
        this.firstName = userInfo.getFirstName();
        this.address = userInfo.getAddress();
        this.city = userInfo.getCity();
        this.state = userInfo.getState();
        this.zipcode = userInfo.getZipcode();
        this.telephone = userInfo.getTelephone();
        this.email = userInfo.getEmail();
    }
    
    @Override
    public String toString() {
        return "Customer: [customerid=" + customerId +  "]";
    }
}
