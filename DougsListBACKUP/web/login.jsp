<%-- 
    Login Page:
        This is where users(customers, managers, customer-representatives) login to their account.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>  
    <head>          
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->        
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Login</title>  
    </head>  
    <body>

        <c:if test="${not empty destination}">
            <jsp:forward page="${destination}"/>
        </c:if>
        
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='./index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello! Please <a href="login.jsp">Log In</a> or <a href="registration.jsp">Register</a>                   
                </div>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span></h1>
            </div>
        </nav>

        <div class="container">
            <div class ='login'>
                <form action="LoginServlet" method="POST">  
                    <fieldset style="width: 300px">  
                        <legend>Login</legend>  
                        <table>  
                            <tr>  
                                <td>Username</td>  
                                <td><input type="text" name="username" required="required" /></td>  
                            </tr>  
                            <tr>  
                                <td>Password</td>  
                                <td><input type="password" name="password" required="required" /></td>  
                            </tr>  
                        </table>  
                        <div class='submit-button'>
                        <input type="submit" value="Login" />
                        </div>
                    </fieldset>  
                </form>  
            </div>
        </div>
    </body>  
</html>  
