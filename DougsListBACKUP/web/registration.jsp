<%-- 
    Registration Page:
        This is where a user can register themselves for a customer account.
        Following a valid registration users will be taken to the Login page.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
    </head>
    <body>  
        
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='./index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello! Please <a href="login.jsp">Log In</a> or <a href="registration.jsp">Register</a>       
                 
                </div>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span></h1>
            </div>
        </nav>
        
        <div class="container">
            <div class ='registration'>
                <form action="RegistrationServlet" method="POST">  
                    <fieldset style="width: 300px">  
                        <legend> Register </legend>  
                        <table>  
                            <tr>  
                                <td>First Name</td>  
                                <td><input type="text" name="firstName" required="required" /></td>  
                            </tr>  
                            <tr>  
                                <td>Last Name</td>  
                                <td><input type="text" name="lastName" required="required" /></td>  
                            </tr>  
                            <tr>  
                                <td>Address</td>  
                                <td><input type="text" name="address" required="required" /></td>  
                            </tr>  
                            <tr>  
                                <td>City</td>  
                                <td><input type="text" name="city" required="required" /></td>  
                            </tr>  
                            <tr>  
                                <td>State</td>  
                                <td><input type="text" name="state" required="required" /></td>  
                            </tr>  
                            <tr>  
                                <td>ZIP Code</td>  
                                <td><input type="text" name="zipcode" required="required" /></td>  
                            </tr>  
                            <tr>  
                                <td>Telephone</td>  
                                <td><input type="text" name="telephone" required="required" /></td>  
                            </tr>
                            <tr>  
                                <td>Email</td>  
                                <td><input type="text" name="email" required="required" /></td>  
                            </tr>
                            <tr>  
                                <td>Credit Card Number</td>
                                <td><input type="text" name="creditCard" required="required" /></td>  
                            </tr> 
                            <tr>  
                                <td>Password</td><!--TODO: Add Re-enter password-->  
                                <td><input type="password" name="password" required="required" /></td>  
                            </tr> 
                            
                        </table>  
                        <div class='submit-button'>
                        <input type="submit" value="Submit"> 
                        </div>
                    </fieldset>  
                </form>  
            </div>
        </div>
    </body>  
</html>