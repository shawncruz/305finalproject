package doug.model;

/**
 *
 * @author ShawnCruz
 */
public class Revenue {
    
    private String name;
    private double revenue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }
}
