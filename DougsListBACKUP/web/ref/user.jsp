<%--
  Created by IntelliJ IDEA.
  User: ShawnCruz
  Date: 11/12/15
  Time: 3:28 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
    <link type="text/css"
          href="css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
    <title>Add new user</title>
</head>
<body>

<form method="POST" action='UserController' name="frmAddUser">
    User ID : <input type="text" readonly="readonly" name="userid" value="<c:out value="${user.userId}" />"> <br>
    First Name : <input type="text" name="firstName" value="<c:out value="${user.firstName}" />"> <br>
    Last Name : <input type="text" name="lastName" value="<c:out value="${user.lastName}" />"> <br>
    Address : <input type="text" name="address" value="<c:out value="${user.address}" />"> <br>
    City : <input type="text" name="city" value="<c:out value="${user.city}" />"> <br>
    State : <input type="text" name="state" value="<c:out value="${user.state}" />"> <br>
    Zip Code : <input type="text" name="zipcode" value="<c:out value="${user.zipcode}" />"> <br>
    Telephone : <input type="text" name="telephone" value="<c:out value="${user.telephone}" />"> <br>
    Email : <input type="text" name="email" value="<c:out value="${user.email}" />"> <br>
    <input type="submit" value="Submit">
</form>
</body>
</html>
