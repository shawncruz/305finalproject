<%-- 
    Manager Page:
        If the user is a manager, this is where they will be taken upon signing
        in from the login page.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>  
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Welcome <c:out value="${currentUser.firstName}"/></title>  
    </head>  

    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>

        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>

        <div class ='container'>
            <h3><c:out value="${currentUser.firstName}"/>'s Account</h3> 
            <hr> 

            <p class="user-info">
                Name: ${currentUser.firstName} ${currentUser.lastName} <br>
                Level: Manager        
                <br>
                Address: ${currentUser.address}, ${currentUser.city}, ${currentUser.state} ${currentUser.zipcode}<br>
                Telephone: ${currentUser.telephone}<br>
                Email: ${currentUser.email}
            </p>
            <br><br>
            <h6> Manager Tools </h6>
            <hr>
            <p>
                <a href="EmployeeController?action=listEmployees">List Employees</a><br>
                <a href="CustomerController?action=listCustomers">List Customers</a><br>
                <a href="ManagerLogicServlet?action=listItems">List Items</a><br>
            </p>

            <h6> Sales </h6>
            <hr>
            <form method="GET" action="ManagerLogicServlet">
                <p> <input style='display:none' name='action' type='text' value='bestSellers'>
                    <input type="submit" value="Best Sellers List"></p>
            </form>
            
            <form method="GET" action="ManagerLogicServlet?action=salesreport">
                
                <p>Obtain Sales Report For Specified Month<br>
                <input style='display:none' name='action' type='text' value='salesreport'><!--A little hack-->
                <select name="monthList">
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
                <input type="submit" value="Submit">
                </p>
            </form>

           <form method="GET" action="ManagerLogicServlet">
               <p>
                <input style='display:none' name='action' type='text' value='salesByItem'>
                Produce List of Sales by Item Name:<br>
                <input type="text" name="itemName">
                <input type="submit" value="Submit">
               </p>
            </form>

            <form method="GET" action="ManagerLogicServlet">
                <p>
                <input style='display:none' name='action' type='text' value='salesByCustomer'>
                Produce List of Sales by Customer Name:<br>
                <input type="text" name="customerName">
                <input type="submit" value="Submit">
                </p>
            </form>
            <h6> Revenue </h6>
            <hr>
            <form method="GET" action="ManagerLogicServlet">
                <p>
                <input style='display:none' name='action' type='text' value='revItem'>
                Revenue Produced by Item Name<br>
                <input type="text" name="itemName">
                <input type="submit" value="Submit">
                </p>
            </form>

            <form method="GET" action="ManagerLogicServlet">
                <p>
                <input style='display:none' name='action' type='text' value='revItemType'>
                Revenue Produced by Item Type<br>
                <input type="text" name="itemType">
                <input type="submit" value="Submit">
                </p>
            </form>

            <form method="GET" action="ManagerLogicServlet">
                <p>
                <input style='display:none' name='action' type='text' value='revCustomer'>
                Revenue Produced by Customer (enter customer name)<br>
                <input type="text" name="customerName">
                <input type="submit" value="Submit">
                </p>
            </form>

            <form method="GET" action="ManagerLogicServlet">
                <p>
                <input style='display:none' name='action' type='text' value='custRepMostRevenue'>
                Determine Customer Rep Who Generated Most Total Revenue <br>
                <input type="submit" value="Click Here">
                </p>
            </form>

            <form method="GET" action="ManagerLogicServlet">
                <p>
                <input style='display:none' name='action' type='text' value='customerMostRevenue'>
                Determine Customer Who Generated Most Total Revenue<br>
                <input type="submit" value="Click Here">
                </p>
            </form>



        </ul>
    </div>
</body>  
</html>  
