<%-- 
    Document   : listEmployee
    Created on : Nov 19, 2015, 5:31:01 PM
    Author     : ShawnCruz
--%>

<%@page import="java.util.List"%>
<%@page import="doug.model.Employee"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Show All Employees</title>  
    </head>  
    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="inner-container">
                <table border=1>
                    <h4>Employee List</h4>
                    <hr>
                    <thead>
                        <tr>
                            <th>Employee Id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zip Code</th>
                            <th>Telephone</th>
                            <th>Email</th>
                            <th>Start Date</th>
                            <th>Hourly Rate</th>
                            <th>Level</th>
                            <th colspan=2>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${employees}" var="employee">
                            <tr>        
                                <td><c:out value="${employee.employeeId}"/></td>
                                <td><c:out value="${employee.firstName}"/></td>
                                <td><c:out value="${employee.lastName}"/></td>
                                <td><c:out value="${employee.address}"/></td>
                                <td><c:out value="${employee.city}"/></td>
                                <td><c:out value="${employee.state}"/></td>
                                <td><c:out value="${employee.zipcode}"/></td>
                                <td><c:out value="${employee.telephone}"/></td>
                                <td><c:out value="${employee.email}"/></td>
                                <td><c:out value="${employee.startDate}"/></td>
                                <td><c:out value="${employee.hourlyRate}"/></td>
                                <td><c:out value="${employee.lvl}"/></td>
                                <td><a href="EmployeeController?action=edit&employeeId=<c:out value="${employee.employeeId}"/>">Update</a></td>
                                <td><a href="EmployeeController?action=delete&employeeId=<c:out value="${employee.employeeId}"/>">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <p><a href="EmployeeController?action=insert">Add Employee</a></p>
            </div>
        </div>
    </body>
</html>