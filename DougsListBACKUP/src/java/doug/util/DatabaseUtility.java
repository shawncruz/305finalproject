package doug.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by ShawnCruz on 11/12/15.
 */
public class DatabaseUtility {

    private static Connection connection = null;

    public static Connection getConnection() {
        if (connection != null)
            return connection;
        else {
            try {
                Properties prop = new Properties();
                InputStream inputStream = DatabaseUtility.class.getClassLoader().getResourceAsStream("/db.properties");
                prop.load(inputStream);
                String driver = prop.getProperty("DRIVER");
                String url = prop.getProperty("URL");
                String user = prop.getProperty("USER");
                String password = prop.getProperty("PASSWORD");
                Class.forName(driver);
                connection = DriverManager.getConnection(url, user, password);
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return connection;
        }

    }
    
    public static void close() {
        try {
           connection.close();
        } catch(SQLException e) {
                e.printStackTrace();
          }
    }
}
