package doug.model;

/**
 *CREATE TABLE `Auction` (
 *      `AuctionID`	INTEGER(9),
 *      `ItemID`	INTEGER NOT NULL,
 *      `Monitor`	INTEGER NOT NULL,
 *      `CurrentWinner`	INTEGER NOT NULL,
 *      `BidIncrement`	NUMERIC(15,2),
 *      `CurrentBid`	NUMERIC(15,2),
 *      `Finished`	BOOL NOT NULL,
 *      `ReservePrice`	NUMERIC(15,2),
 *  PRIMARY KEY (`AuctionID`), 
 *  FOREIGN KEY(`CurrentWinner`) REFERENCES `Customer` (`CustomerID`)
 *		ON DELETE NO ACTION
 *		ON UPDATE CASCADE,
 *  FOREIGN KEY (`Monitor`) REFERENCES `Employee` (`EmployeeID`)
 *		ON DELETE NO ACTION
 *		ON UPDATE CASCADE,
 *  FOREIGN KEY (`ItemID`) REFERENCES `Item` (`ItemID`)
 *		ON DELETE NO ACTION
 *		ON UPDATE CASCADE,
 *       CHECK (`AuctionID` > 0 AND `AuctionID` < 1000000000)	);
 * 
 * @author ShawnCruz
 */
public class Auction {
    
    private int auctionId;
    private int itemId;
    private int monitor;
    private int currentWinner;
    private double bidIncrement;
    private double currentBid;
    private boolean finished;
    private double reservePrice;

    public int getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(int auctionId) {
        this.auctionId = auctionId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getMonitor() {
        return monitor;
    }

    public void setMonitor(int monitor) {
        this.monitor = monitor;
    }

    public int getCurrentWinner() {
        return currentWinner;
    }

    public void setCurrentWinner(int currentWinner) {
        this.currentWinner = currentWinner;
    }

    public double getBidIncrement() {
        return bidIncrement;
    }

    public void setBidIncrement(double bidIncrement) {
        this.bidIncrement = bidIncrement;
    }

    public double getCurrentBid() {
        return currentBid;
    }

    public void setCurrentBid(double currentBid) {
        this.currentBid = currentBid;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
    
    public double getReservePrice() {
        return reservePrice;
    }

    public void setReservePrice(double reservePrice) {
        this.reservePrice = reservePrice;
    }
}
