<%--
  Created by IntelliJ IDEA.
  User: ShawnCruz
  Date: 11/12/15
  Time: 3:10 AM
  To change this template use File | Settings | File Templates.
--%>
<%@page import="java.util.List"%>
<%@page import="doug.model.User"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
    <title>Show All Users</title>
</head>
<body>
<table border=1>
    <thead>
    <tr>
        <th>User Id</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Address</th>
        <th>City</th>
        <th>State</th>
        <th>Zip Code</th>
        <th>Telephone</th>
        <th>Email</th>
        <th colspan=2>Action</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach items="${users}" var="user">
        <tr>        
            <td><c:out value="${user.userId}"/></td>
            <td><c:out value="${user.firstName}"/></td>
            <td><c:out value="${user.lastName}"/></td>
            <td><c:out value="${user.address}"/></td>
            <td><c:out value="${user.city}"/></td>
            <td><c:out value="${user.state}"/></td>
            <td><c:out value="${user.zipcode}"/></td>
            <td><c:out value="${user.telephone}"/></td>
            <td><c:out value="${user.email}"/></td>
            <td><a href="UserController?action=edit&userId=<c:out value="${user.userId}"/>">Update</a></td>
            <td><a href="UserController?action=delete&userId=<c:out value="${user.userId}"/>">Delete</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<p><a href="UserController?action=insert">Add User</a></p>
</body>
</html>
