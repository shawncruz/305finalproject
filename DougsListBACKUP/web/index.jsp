<%--
  Home Page:
    Users may view this page without being logged in. This is for curious
    potential customers that would just like to browse.

  USE THESE IN A PINCH: out.println("<p></p>");

  Goes in between body tags: <jsp:forward page="/UserController?action=listUser"/>
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>  
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Doug's List</title>  
    </head>  
    <body> 

        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>               
                    <!--- Display "Log In or Sign Up" welcome message only if no user is currently logged in --->
                    <c:if test="${empty currentUser}">
                        <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                        <c:out value="Hello! Please <a href='login.jsp'>Log In</a> or <a href='registration.jsp'>Register</a>"  escapeXml="false"/>
                    </c:if>
                    <!--- Else, display welcome message containing user's name, user icon (to access user page), and logout button --->
                    <c:if test="${!empty currentUser}">
                        <c:out value="
                              <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                              <div class='account'><a href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                              <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                               Hello, ${currentUser.firstName}"  
                               escapeXml="false"/>
                        
                    </c:if>  
                </div>
            </div>
        </nav>

        <div class='container'>
            <div class='search'>
                        <form method="GET" action="CustomerController">
            <input style='display:none' name='action' type='text' value='itemsByKeyword'>
            <input type="text" name="keyword" placeholder="Search Doug's List...">
            <input type="submit" value="Submit">
        </form>
            </div>
        </div>
    </body>  
</html>  

