package doug.dao;

import doug.model.Auction;
import doug.model.Sale;
import doug.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ShawnCruz
 */
public class SalesDAO {
    
    private Connection connection;
    private AuctionDAO auctionDAO;
    private ItemDAO itemDao;

    public SalesDAO() {
        connection = DatabaseUtility.getConnection();
        auctionDAO = new AuctionDAO();
        itemDao = new ItemDAO();
    }
    
    public void recordSale(int auctionId) {
        auctionDAO.setFinished(auctionId);
    }
    
    public List<Auction> getAuctions() {
        return auctionDAO.getOngoingAuctions();
    }
    
    public List<Sale> getSalesByItem(String itemName) {
        List<Sale> salesByItem = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT Item.ItemName AS SoldItem, User.FirstName AS WinnerFirstName,\n" +
                                    "User.LastName AS WinnerLastName, Auction.CurrentBid AS SellingPrice\n" +
                                    "FROM Item, User, Auction, Customer, Post\n" +
                                    "WHERE Auction.Finished = True AND Auction.AuctionID = Post.AuctionID AND Auction.ItemID = Item.ItemID AND \n" +
                                    "Auction.CurrentWinner = Customer.CustomerID AND User.UserID = Customer.CustomerID AND Item.ItemName = ?;");

            preparedStatement.setString(1, itemName);
            ResultSet rs = preparedStatement.executeQuery();
            
            while (rs.next()) {
                Sale sale = new Sale();
                sale.setItemName(rs.getString("SoldItem"));
                sale.setFirstName(rs.getString("WinnerFirstName"));
                sale.setLastName(rs.getString("WinnerLastName"));
                sale.setSellingPrice(rs.getInt("SellingPrice"));
                salesByItem.add(sale);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return salesByItem;
    }
    
        public List<Sale> getSalesByCustomer(String customerName) {
        List<Sale> salesByCustomer = new ArrayList<>();
        String firstName = customerName.split(" ")[0];
        String lastName = customerName.split(" ")[1];
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT Item.ItemName AS SoldItem, User.FirstName AS SellerFirstName,\n" +
                                    "User.LastName AS SellerLastName, Auction.CurrentBid AS SellingPrice\n" +
                                    "FROM Item, User, Auction, Customer, Post\n" +
                                    "WHERE Auction.Finished = True AND Auction.AuctionID = Post.AuctionID AND Auction.ItemID = Item.ItemID AND \n" +
                                    "Post.SellerID = Customer.CustomerID AND User.UserID = Customer.CustomerID AND User.FirstName = ?\n" +
                                    "AND User.LastName = ?;");

            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            ResultSet rs = preparedStatement.executeQuery();
            
            while (rs.next()) {
                Sale sale = new Sale();
                sale.setItemName(rs.getString("SoldItem"));
                sale.setFirstName(rs.getString("SellerFirstName"));
                sale.setLastName(rs.getString("SellerLastName"));
                sale.setSellingPrice(rs.getInt("SellingPrice"));
                salesByCustomer.add(sale);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return salesByCustomer;
    }
    
    public List<Sale> getSalesReport(int month) {
        
        List<Sale> salesReport = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT Auction.ItemID, Item.ItemName, User.FirstName, User.LastName, Auction.CurrentBid AS SellingPrice\n" +
                                     "FROM Auction, Item, Post, Customer, User\n" +
                                     "WHERE Customer.CustomerID = Auction.CurrentWinner AND Customer.CustomerID = User.UserID AND\n" +
                                     "Auction.Finished = TRUE AND Auction.AuctionID = Post.AuctionID AND MONTH(Post.ExpireDate) = ? AND \n" +
                                     "Item.ItemID = Auction.ItemID;");
            
            preparedStatement.setInt(1, month);
            ResultSet rs = preparedStatement.executeQuery();
            
            while (rs.next()) {
                Sale sale = new Sale();
                sale.setAuctionId(rs.getInt("ItemID"));
                sale.setItemName(rs.getString("ItemName"));
                sale.setFirstName(rs.getString("FirstName"));
                sale.setLastName(rs.getString("LastName"));
                sale.setSellingPrice(rs.getInt("SellingPrice"));
                salesReport.add(sale);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return salesReport;
    }
}
