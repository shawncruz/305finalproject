package doug.model;

/**
 *CREATE TABLE `Item` (
 *      `ItemID`        INTEGER, 
 *      `Description` 	CHAR(255), 
 *      `ItemName` 	CHAR(255), 
 *      `Year`		INTEGER(4), # FLAG: Is this attribute necessary?
 *      `ItemType`	CHAR(35), 
 *      `AmountInStock` INTEGER, 
 *      `CopiesSold` 	INTEGER, 
 *  PRIMARY KEY (`ItemID`) ,
 *  CHECK (`ItemID` > 0 AND `ItemID` < 1000000000),
 *  CHECK (`AmountInStock` >= 0 AND `CopiesSold` >= 0)	);
 * 
 * @author ShawnCruz
 */
public class Item {

    private int itemId;
    private String description;
    private String itemName;
    private int year;
    private String itemType;
    private int amountInStock;
    private int copiesSold;
    
    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public int getAmountInStock() {
        return amountInStock;
    }

    public void setAmountInStock(int amountInStock) {
        this.amountInStock = amountInStock;
    }

    public int getCopiesSold() {
        return copiesSold;
    }

    public void setCopiesSold(int copiesSold) {
        this.copiesSold = copiesSold;
    }
}
