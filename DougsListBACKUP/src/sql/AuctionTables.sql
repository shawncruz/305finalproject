# == CREATE TABLES HERE == #
#					       #
#   					   #
#					  	   #
############################

DROP DATABASE IF EXISTS auction;

CREATE DATABASE auction;

USE auction;

CREATE TABLE `User` ( 
`UserID`	INTEGER NOT NULL AUTO_INCREMENT,
`Password`	CHAR(32) NOT NULL,
`LastName`	CHAR(20) NOT NULL,
`FirstName`	CHAR(20) NOT NULL,
`Address`	CHAR(30),
`City`		CHAR(30),
`State`		CHAR(30),
`ZipCode`	INTEGER,
`Telephone`	VARCHAR(20),
`Email`		VARCHAR(30),
PRIMARY KEY (`UserID`)	);

ALTER TABLE `User` AUTO_INCREMENT=10000000;

CREATE TABLE `Customer` (
`CustomerID`	INTEGER NOT NULL,
`Rating`		CHAR,
`CreditCardNum`	VARCHAR(16),
PRIMARY KEY (`CustomerID`),
FOREIGN KEY (`CustomerID`) REFERENCES `User` (`UserID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE);


# Question: Do we want to delete the corresponding User entry?
CREATE TABLE `Employee` (
`EmployeeID`	INTEGER NOT NULL,
`StartDate`		DATETIME,
`HourlyRate`	NUMERIC(15,2),
`Lvl`			INTEGER,
PRIMARY KEY (`EmployeeID`),
FOREIGN KEY (`EmployeeID`) REFERENCES `User` (`UserID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE);
        
        
CREATE TABLE `Item` (
`ItemID` 		INTEGER NOT NULL AUTO_INCREMENT, 
`Description` 	CHAR(255), 
`ItemName` 			CHAR(255), 
`Year`			INTEGER(4), # FLAG: Is this attribute necessary?
`ItemType`	 		CHAR(35), 
`AmountInStock` 	INTEGER, 
`CopiesSold` 	INTEGER, 
PRIMARY KEY (`ItemID`) ,
CHECK (`ItemID` > 0 AND `ItemID` < 1000000000),
CHECK (`AmountInStock` >= 0 AND `CopiesSold` >= 0)	);

ALTER TABLE `Item` AUTO_INCREMENT=1;

# An auction will be the most updated Bid entry
# @TODO: An auctions may last 3, 5, or 7 days from the time the auction is opened
CREATE TABLE `Auction` (
`AuctionID`			INTEGER(9) AUTO_INCREMENT,
`ItemID`			INTEGER NOT NULL,
`Monitor`			INTEGER NOT NULL,
`CurrentWinner`		INTEGER,
`BidIncrement`		NUMERIC(15,2),
`CurrentBid`		NUMERIC(15,2),
`Finished`			BOOL NOT NULL,
`ReservePrice`		NUMERIC(15,2),
PRIMARY KEY (`AuctionID`),
FOREIGN KEY(`CurrentWinner`) REFERENCES `Customer` (`CustomerID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE,
FOREIGN KEY (`Monitor`) REFERENCES `Employee` (`EmployeeID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE,
FOREIGN KEY (`ItemID`) REFERENCES `Item` (`ItemID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE,
        CHECK (`AuctionID` > 0 AND `AuctionID` < 1000000000)	);
        
ALTER TABLE `Auction` AUTO_INCREMENT=1;


CREATE TABLE `Post` ( 
`ExpireDate`		DATETIME,
`PostDate`			DATETIME,
`SellerID`			INTEGER NOT NULL,
`AuctionID`			INTEGER(9),
PRIMARY KEY (`SellerID`, `AuctionID`),
FOREIGN KEY (`SellerID`) REFERENCES `Customer` (`CustomerID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE,
FOREIGN KEY (`AuctionID`) REFERENCES `Auction` (`AuctionID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE ,
        CHECK (`AuctionID` > 0 AND `AuctionID` < 1000000000)	);

# An Auction has many bids
CREATE TABLE `Bid` (
`BidderID`			INTEGER NOT NULL,
`BidPlaced`			NUMERIC(15,2),
`CurrentMaxBid`		NUMERIC(15,2),
`AuctionID`			INTEGER(9),
`ItemID`			INTEGER(9),
`BidTime`			DATETIME,
PRIMARY KEY (`BidderID`, `ItemID`, `BidTime`),
FOREIGN KEY(`BidderID`) REFERENCES `Customer` (`CustomerID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE,
FOREIGN KEY(`ItemID`) REFERENCES `Item` (`ItemID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE,
FOREIGN KEY(`AuctionID`) REFERENCES `Auction` (`AuctionID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE );

# For creating a mailing list for a particular employee        
CREATE TABLE `MailingList` (
`Monitor` INTEGER NOT NULL,
`Customer` INTEGER NOT NULL,
`Email` VARCHAR(30),
	PRIMARY KEY(`Monitor`,`Customer`),
	FOREIGN KEY(`Monitor`) REFERENCES `Employee` (`EmployeeID`),
	FOREIGN KEY(`Customer`) REFERENCES `Customer` (`CustomerID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE
);