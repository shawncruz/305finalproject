package doug.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import doug.dao.LoginDAO;
import doug.model.Customer;
import doug.model.Employee;
import doug.model.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ShawnCruz
 */
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static String INDEX_PAGE = "./index.jsp";
    private static String LOGIN_PAGE = "/login.jsp";
    private static String DESTINATION_PAGE = "./error.jsp";
    private static String CUSTOMER_PAGE = "./customer.jsp";
    private static String MANAGER_PAGE = "./manager.jsp";
    private static String REPRESENTATIVE_PAGE = "./representative.jsp";
    private LoginDAO dao;
    
    public LoginServlet() {
        super();
        dao = new LoginDAO();
    }
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(false);
        
        String username = request.getParameter("username"); // username = email
        String password = request.getParameter("password");
        
        RequestDispatcher view;
        
        if(dao.validate(username, password)) {
            
            User currentUser = dao.getUser(username);
            
            DESTINATION_PAGE = determineDestination(currentUser, session);
            
            session.setAttribute("destination", DESTINATION_PAGE);
            response.sendRedirect(INDEX_PAGE);
            //view = request.getRequestDispatcher(DESTINATION_PAGE);
            //view.forward(request, response);
        }    
        else {    
            
            out.print("<p style=\"color:red\">Username or password incorrect. Please try again.</p>");    
            view = request.getRequestDispatcher(LOGIN_PAGE);    
            view.include(request,response);    
        }    
  
        out.close(); 
    }
    
    private String determineDestination(User user, HttpSession session) {
        String destination = "";
        if(user instanceof Customer) {
                Customer customer = (Customer) user;
                session.setAttribute("currentUser", customer);
                destination = CUSTOMER_PAGE;
        } 
        else if(user instanceof Employee) {
                Employee employee = (Employee) user;
                session.setAttribute("currentUser", employee);
                int level = employee.getLvl();
                if(level == 0) {
                    destination = REPRESENTATIVE_PAGE;
                } else {
                    destination = MANAGER_PAGE;
                }
            }
        return destination;
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
