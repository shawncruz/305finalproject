package doug.model;

/**
 * Created by ShawnCruz on 11/12/15.
 *
 * POJ for the User table in the DB
 *
 * Create Statement:
 *
 * CREATE TABLE `User` (
 *      `UserID`	INTEGER NOT NULL AUTO_INCREMENT,
 *      `Password`	CHAR(32) NOT NULL,
 *      `LastName`	CHAR(20) NOT NULL,
 *      `FirstName`	CHAR(20) NOT NULL,
 *      `Address`	CHAR(30),
 *      `City`		CHAR(30),
 *      `State`		CHAR(30),
 *      `ZipCode`	INTEGER,
 *      `Telephone`	VARCHAR(20),
 *      `Email`		VARCHAR(30),
 *  PRIMARY KEY (`UserID`)	);
 */
public class User {

    protected int userId;
    protected String password;
    protected String lastName;
    protected String firstName;
    protected String address;
    protected String city;
    protected String state;
    protected int zipcode;
    protected String telephone;
    protected String email;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User [userid=" + userId + ", firstName=" + firstName
                + ", lastName=" + lastName + ", address=" + address + ", city=" + city
                + ", state=" + zipcode + ", telephone=" + telephone + ", email=" + email + "]";
    }
}
