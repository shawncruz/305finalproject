<%-- 
    Document   : viewAuction
    Created on : Dec 2, 2015, 1:51:02 AM
    Author     : Lauren
--%>

<%@page import="java.util.List"%>
<%@page import="doug.model.Employee"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>View Auction</title>  
    </head> 
    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>
        <div class ="container">
            <div class="auction-container">  
                <h3>Viewing Auction for ${item.itemName}</h3>
                <hr>
                <table class="auction-table">
                    <tr>
                        <td>Seller:</td>
                        <td>${seller.firstName} ${seller.lastName}</td>
                    </tr>
                    <tr>
                        <td>Item Name:</td>
                        <td>${item.itemName}</td>
                    </tr>
                    <tr>
                        <td>Description:</td>
                        <td>${item.description}</td>
                    </tr>
                    <tr>
                        <td>Year: </td>
                        <td>${item.year}</td>
                    </tr>
                    <tr>
                        <td>Reserve Price: </td>
                        <td><fmt:formatNumber value="${auction.reservePrice}" type="currency"/></td>
                    </tr>
                    <tr>
                        <td>Auction Start: </td>
                        <td>${post.postDate}</td>
                    </tr>
                    <tr>
                        <td>Auction End: </td>
                        <td>${post.expireDate}</td>
                    </tr>
                    <tr>
                        <td>Current Bid:</td>
                        <td> <fmt:formatNumber value="${auction.currentBid}" type="currency"/></td>
                    </tr>
                    <tr>
                        <td>Closed?:</td>
                        <td>                
                            <c:if test="${auction.finished==false}">
                                <c:out value="No"/>
                            </c:if>
                            <c:if test="${auction.finished==true}">
                                <c:out value="Yes"/>
                            </c:if>
                        </td>
                    </tr>
                </table>
                    <br><br>
              <c:if test="${auction.finished==false}">
                  Place a bid:
                   <form method="POST" action="BidServlet">
                    <input style='display:none' name='bidderId' type='text' value='${currentUser.userId}'>
                    <input style='display:none' name='auctionId' type='text' value='${auction.auctionId}'>
                    <input style='display:none' name='itemId' type='text' value='${item.itemId}'>
                    <input type="text" name="bid">
                    <input type="submit" value="Submit">
                </form>
             </c:if>

                <p>
                 <h3>Bid History</h3>
                 <hr>
                <table width="50%" border=1>
                    <thead>
                        <tr>
                            <th>Bidder Id</th>
                            <th>Bid</th>   
                            <th>Time</th>   
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${bids}" var="bid">
                            <tr>
                                <td><c:out value="${bid.bidderId}"/></td>
                                <td><fmt:formatNumber value="${bid.bidPlaced}" type="currency"/></td>
                                <td><c:out value="${bid.bidTime}"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>