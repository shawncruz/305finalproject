package doug.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import doug.model.User;
import doug.util.DatabaseUtility;

/**
 * Created by ShawnCruz on 11/12/15.
 *
 * Data Access Object
 * Contains the logic for DB operations on User
 *
 */
public class UserDAO {

    private Connection connection;

    public UserDAO() {
        connection = DatabaseUtility.getConnection();
    }
    /**
     * This method solves the issue with dynamically created id's that are only present
     * in the DB, allowing a direct mapping between a corresponding Customer and User.
     * @param user
     * @return The resulting User entry in the database
     */
    public User addUser(User user) {
        User resultantUser = new User();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO User(Password, LastName, FirstName, Address, City, State, ZipCode, Telephone, Email) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? )");
            String userEmail = user.getEmail();
            preparedStatement.setString(1, user.getPassword());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getFirstName());
            //preparedStatement.setDate(, new java.sql.Date(user.getDob().getTime())); MAY NEED THIS LATER
            preparedStatement.setString(4, user.getAddress());
            preparedStatement.setString(5, user.getCity());
            preparedStatement.setString(6, user.getState());
            preparedStatement.setInt(7, user.getZipcode());
            preparedStatement.setString(8, user.getTelephone());
            preparedStatement.setString(9, userEmail);
            preparedStatement.executeUpdate();
            
            resultantUser = getUserByEmail(userEmail);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultantUser;
    }

    public void deleteUser(int userId) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM User WHERE UserID=?");
            // Parameters start with 1
            preparedStatement.setInt(1, userId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUser(User user) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE User SET LastName=?, FirstName=?, Address=?, City=?, " +
                            "State=?, ZipCode=?, Telephone=?, Email=? WHERE UserID=?");
            // Parameters start with 1
            preparedStatement.setString(1, user.getLastName());
            preparedStatement.setString(2, user.getFirstName());
            //preparedStatement.setDate(3, new java.sql.Date(user.getDob().getTime())); MAY NEED THIS LATER
            preparedStatement.setString(3, user.getAddress());
            preparedStatement.setString(4, user.getCity());
            preparedStatement.setString(5, user.getState());
            preparedStatement.setInt(6, user.getZipcode());
            preparedStatement.setString(7, user.getTelephone());
            preparedStatement.setString(8, user.getEmail());
            preparedStatement.setInt(9, user.getUserId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM User");
            while (rs.next()) {
                User user = new User();
                user.setUserId(rs.getInt("UserID"));
                user.setPassword(rs.getString("Password"));
                user.setLastName(rs.getString("LastName"));
                user.setFirstName(rs.getString("FirstName"));
                user.setAddress(rs.getString("Address"));
                user.setCity(rs.getString("City"));
                user.setState(rs.getString("State"));
                user.setZipcode(rs.getInt("ZipCode"));
                user.setTelephone(rs.getString("Telephone"));
                user.setEmail(rs.getString("Email"));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    public User getUserById(int userId) {
        User user = new User();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT * FROM User WHERE UserID=?");
            preparedStatement.setInt(1, userId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                user.setUserId(rs.getInt("UserID"));
                user.setPassword(rs.getString("Password"));
                user.setLastName(rs.getString("LastName"));
                user.setFirstName(rs.getString("FirstName"));
                user.setAddress(rs.getString("Address"));
                user.setCity(rs.getString("City"));
                user.setState(rs.getString("State"));
                user.setZipcode(rs.getInt("ZipCode"));
                user.setTelephone(rs.getString("Telephone"));
                user.setEmail(rs.getString("Email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }
    
    public User getUserByEmail(String userEmail) {
        User user = new User();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT * FROM User WHERE Email=?");
            preparedStatement.setString(1, userEmail);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                user.setUserId(rs.getInt("UserID"));
                user.setPassword(rs.getString("Password"));
                user.setLastName(rs.getString("LastName"));
                user.setFirstName(rs.getString("FirstName"));
                user.setAddress(rs.getString("Address"));
                user.setCity(rs.getString("City"));
                user.setState(rs.getString("State"));
                user.setZipcode(rs.getInt("ZipCode"));
                user.setTelephone(rs.getString("Telephone"));
                user.setEmail(rs.getString("Email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }
}
