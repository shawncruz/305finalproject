305 Use Cases

#####################Pages#####################

Home Page: Users can search and view posts without having to login. Login and register links should be available here.

Login Page: Customer or Manager logs in.

Registration Page: Only for Customers. Users create Customer accounts.

Customer Profile Page: page where Customer can manage their activity

Manager Profile Page: page where Manager can manage auctions and possibly other managers.

Item/Post Page: page where users can view an item/post and its current auction info

Create Post Page: customer creates a post for auction here.