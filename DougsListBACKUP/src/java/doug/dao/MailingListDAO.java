package doug.dao;

import doug.model.Customer;
import doug.model.Employee;
import doug.model.MailingEntry;
import doug.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ShawnCruz
 */
public class MailingListDAO {
    
    private Connection connection;
    private CustomerDAO customerDAO;
    private EmployeeDAO employeeDAO;

    public MailingListDAO() {
        connection = DatabaseUtility.getConnection();
        customerDAO = new CustomerDAO();
        employeeDAO = new EmployeeDAO();
    }
    
    public void addCustomerToList(int customerId, int employeeId) {
        
        try {     
            Customer customer = customerDAO.getCustomerById(customerId);
            String email = customer.getEmail();
            PreparedStatement preparedStatement = connection.
                    prepareStatement("INSERT INTO MailingList (Monitor, Customer, Email) VALUES (?, ?, ?)");

            preparedStatement.setInt(1, employeeId);
            preparedStatement.setInt(2, customerId);
            preparedStatement.setString(3, email);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } 
    }
    
    public void deleteRecipient(int customerId) {
         try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM MailingList WHERE Customer=?");
            // Parameters start with 1
            preparedStatement.setInt(1, customerId);
            preparedStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List<MailingEntry> getMailingList(int employeeId) {
        
        List<MailingEntry> mailingList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT MailingList.Monitor, MailingList.Customer, MailingList.Email\n" +
                                    "FROM User, Customer, Employee, MailingList\n" +
                                    "WHERE Employee.EmployeeID = ? AND Employee.EmployeeID = MailingList.Monitor\n" +
                                    "AND MailingList.Email = User.Email AND MailingList.Customer = Customer.CustomerID;");
            
            preparedStatement.setInt(1, employeeId);
            ResultSet rs = preparedStatement.executeQuery();
            
            while (rs.next()) {
                MailingEntry entry = new MailingEntry();
                entry.setMonitor(rs.getInt("Monitor"));
                entry.setCustomer(rs.getInt("Customer"));
                entry.setEmail(rs.getString("Email"));
                mailingList.add(entry);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return mailingList;
    }
}
