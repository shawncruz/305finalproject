package doug.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import doug.dao.CustomerDAO;
import doug.model.Customer;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ShawnCruz
 */
public class RegistrationServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final String DEFAULT_RATING = "0";
    private static String LOGIN_PAGE = "./login.jsp";
    private CustomerDAO dao;
    
    public RegistrationServlet() {
        super();
        dao = new CustomerDAO();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
        Customer customer = new Customer();
        customer.setPassword(request.getParameter("password"));
        customer.setLastName(request.getParameter("lastName"));
        customer.setFirstName(request.getParameter("firstName"));
        customer.setAddress(request.getParameter("address"));
        customer.setCity(request.getParameter("city"));
        customer.setState(request.getParameter("state"));
        customer.setZipcode(Integer.parseInt(request.getParameter("zipcode")));
        customer.setTelephone(request.getParameter("telephone"));
        customer.setEmail(request.getParameter("email"));
        customer.setRating(DEFAULT_RATING);
        customer.setCreditCard(request.getParameter("creditCard"));
        
        dao.addCustomer(customer);
        
        //RequestDispatcher view = request.getRequestDispatcher(LOGIN_PAGE);
        //view.forward(request, response);
        response.sendRedirect(LOGIN_PAGE);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
