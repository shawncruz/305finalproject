<%-- 
    Document   : createAuction
    Created on : Nov 24, 2015, 8:37:01 PM
    Author     : Lauren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Add an Item</title>  
    </head>  
    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="inner-container">
                <form action="ItemServlet" method="POST">  
                    <fieldset style="width: 300px">  
                        <legend>Add An Item </legend>  
                        The item you are trying to sell does not exist in our database yet. Please tell us a little
                        bit about it.<br><br>
                        <table>  
                            <tr>  
                                <td>Item Name</td>  
                                <td><input type="text" value="${itemName}" name="itemName" required="required" readonly="true"/></td>  
                            </tr>  
                            <tr>  
                                <td>Item Description</td>  
                                <td><input type="text" name="itemDescription" required="required" /></td>  
                            </tr>
                            <tr>  
                                <td>Year</td>  
                                <td><input type="text" name="itemYear" required="required" /></td>  
                            </tr>  
                            <tr>  
                                <td>Item Type (e.g Car, DVD)</td>  
                                <td><input type="text" name="itemType" required="required" /></td>  
                            </tr>  
                        </table>  
                        <div class="submit-button">
                            <td><input type="submit" value="Submit" /></td>
                        </div>
                    </fieldset>  
                </form>  
            </div>
        </div>
    </body>
</html>
