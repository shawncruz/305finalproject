<%-- 
    Document   : listCustomer
    Created on : Nov 20, 2015, 11:44:34 PM
    Author     : ShawnCruz
--%>

<%@page import="java.util.List"%>
<%@page import="doug.model.Employee"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Show All Customers</title>  
    </head>  
    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="inner-container">
                <table border=1>
                    <h4>Customer List</h4>
                    <hr>
                    <thead>
                        <tr>
                            <th>Customer Id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zip Code</th>
                            <th>Telephone</th>
                            <th>Email</th>
                            <th>Rating</th>
                            <th>Credit Card Number</th>
                            <th colspan=2>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${customers}" var="customer">
                            <tr>        
                                <td><c:out value="${customer.customerId}"/></td>
                                <td><c:out value="${customer.firstName}"/></td>
                                <td><c:out value="${customer.lastName}"/></td>
                                <td><c:out value="${customer.address}"/></td>
                                <td><c:out value="${customer.city}"/></td>
                                <td><c:out value="${customer.state}"/></td>
                                <td><c:out value="${customer.zipcode}"/></td>
                                <td><c:out value="${customer.telephone}"/></td>
                                <td><c:out value="${customer.email}"/></td>
                                <td><c:out value="${customer.rating}"/></td>
                                <td><c:out value="${customer.creditCard}"/></td>
                                <td><a href="CustomerController?action=edit&customerId=<c:out value="${customer.customerId}"/>">Update</a></td>
                                <td><a href="CustomerController?action=delete&customerId=<c:out value="${customer.customerId}"/>">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <p><a href="CustomerController?action=insert">Add Customer</a></p>
            </div>
        </div>
    </body>
</html>
