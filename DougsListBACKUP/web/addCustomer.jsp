<%-- 
    Document   : addCustomer
    Created on : Nov 20, 2015, 11:44:47 PM
    Author     : ShawnCruz
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>New Customer</title>  
    </head>  
    <body>
        <c:if test="${empty currentUser}">
            <jsp:forward page="./login.jsp"/>
        </c:if>
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="registration">

                <form method="POST" action='CustomerController' name="frmAddCustomer">
                    <fieldset style="width: 400px"> 
                        <table>
                            <c:if test="${!empty customer.customerId}">
                                <tr>
                                    <td>Customer ID (Read-Only): </td>
                                    <td><input type="text" readonly="readonly" name="customerId" value="<c:out value="${customer.customerId}" />"></td>
                                </tr>
                            </c:if>
                            <tr>
                                <td>Password: </td>
                                <td><input type="password" name="password" value="<c:out value="${customer.password}" />"> </td>
                            </tr>
                            <tr>
                                <td>First Name: </td>
                                <td><input type="text" name="firstName" value="<c:out value="${customer.firstName}" />"> </td>
                            </tr>
                            <tr>
                                <td>Last Name: </td>
                                <td><input type="text" name="lastName" value="<c:out value="${customer.lastName}" />"> </td>
                            </tr>
                            <tr>
                                <td>Address: </td>
                                <td><input type="text" name="address" value="<c:out value="${customer.address}" />"> </td>
                            </tr>
                            <tr>
                                <td>City:</td>
                                <td><input type="text" name="city" value="<c:out value="${customer.city}" />"> </td>
                            </tr>
                            <tr>
                                <td>State: </td>
                                <td><input type="text" name="state" value="<c:out value="${customer.state}" />"> </td>
                            </tr>
                            <tr>
                                <td>Zip Code: </td>
                                <td><input type="text" name="zipcode" value="<c:out value="${customer.zipcode}" />"> </td>
                            </tr>
                            <tr>
                                <td>Telephone: </td>
                                <td><input type="text" name="telephone" value="<c:out value="${customer.telephone}" />"> </td>
                            </tr>
                            <tr>
                                <td>Email: </td>
                                <td><input type="text" name="email" value="<c:out value="${customer.email}" />"> </td>
                            </tr>
                            <tr>
                                <td>    Rating:</td>
                                <td><input type="text" name="rating" value="<c:out value="${customer.rating}" />"> </td>
                            </tr>
                            <tr>
                                <td>Credit Card Number: </td>
                                <td><input type="text" name="creditCardNum" value="<c:out value="${customer.creditCard}" />"> </td>
                            </tr>
                            <tr>
                        </table>
                                                        <div class="submit-button">
                                <input type="submit" value="Submit">
                            </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>
