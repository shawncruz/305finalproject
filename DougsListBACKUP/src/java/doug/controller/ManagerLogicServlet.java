package doug.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import doug.dao.ItemDAO;
import doug.dao.RevenueDAO;
import doug.dao.SalesDAO;
import doug.model.Revenue;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormatSymbols;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ShawnCruz
 */
public class ManagerLogicServlet extends HttpServlet {

    private static String BEST_SELLERS = "/bestSellers.jsp";
    
    private static String REV_BY_ITEM = "/revenue/revByItem.jsp";
    private static String REV_BY_ITEM_TYPE = "/revenue/revByItemType.jsp";
    private static String REV_BY_CUSTOMER = "/revenue/revByCustomer.jsp";
    private static String REV_BY_CUST_REP_MAX = "/revenue/revByCustRepMax.jsp";
    private static String REV_BY_CUST_MAX = "/revenue/revByCustomerMax.jsp";
    
    private static String SALES_REPORT = "/sales/salesreport.jsp";
    private static String SALES_BY_ITEM = "/sales/salesByItem.jsp";
    private static String SALES_BY_CUSTOMER = "/sales/salesByCustomer.jsp";
    
    private static String LIST_ITEM = "/listItems.jsp";
    
    private static String ERROR = "/error.jsp";
    
    private ItemDAO itemDAO;
    private SalesDAO salesDAO;
    private RevenueDAO revDAO;

    public ManagerLogicServlet() {
        super();
        itemDAO = new ItemDAO();
        salesDAO = new SalesDAO();
        revDAO = new RevenueDAO();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String forward="";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("salesreport")){
            
            forward = SALES_REPORT;
            int month = Integer.parseInt(request.getParameter("monthList"));
            String monthString = new DateFormatSymbols().getMonths()[month-1];
            request.setAttribute("monthString", monthString);
            request.setAttribute("salesReport", salesDAO.getSalesReport(month));
            
        } else if (action.equalsIgnoreCase("listItems")){
            
            forward = LIST_ITEM;
            request.setAttribute("items", itemDAO.getAllItems());
            request.getSession().setAttribute("items", itemDAO.getAllItems());
            
        } else if (action.equalsIgnoreCase("salesByCustomer")){
            
            forward = SALES_BY_CUSTOMER;
            String customerName = request.getParameter("customerName");
            request.setAttribute("customerName", customerName);
            request.setAttribute("salesByCustomer", salesDAO.getSalesByCustomer(customerName));
            
        } else if (action.equalsIgnoreCase("salesByItem")){
            
            forward = SALES_BY_ITEM;
            String itemName = request.getParameter("itemName");
            request.setAttribute("itemName", itemName);
            request.setAttribute("salesByItem", salesDAO.getSalesByItem(itemName));
            
        } else if (action.equalsIgnoreCase("revItem")){
            
            forward = REV_BY_ITEM;
            String itemName = request.getParameter("itemName");
            request.setAttribute("itemName", itemName);
            request.setAttribute("revByItem", revDAO.getRevenueByItem(itemName));
            
        }else if (action.equalsIgnoreCase("revItemType")){
            
            forward = REV_BY_ITEM_TYPE;
            String itemType = request.getParameter("itemType");
            request.setAttribute("itemType", itemType);
            request.setAttribute("revByItemType", revDAO.getRevenueByItemType(itemType));
            
        }else if (action.equalsIgnoreCase("revCustomer")){
            
            forward = REV_BY_CUSTOMER;
            String customerName = request.getParameter("customerName");
            request.setAttribute("customerName", customerName);
            request.setAttribute("revByCustomer", revDAO.getRevenueByCustomer(customerName));
            
        } else if (action.equalsIgnoreCase("custRepMostRevenue")){
            
            forward = REV_BY_CUST_REP_MAX;
            request.setAttribute("custRepMax", revDAO.getCustRepMaxRev());
            
        } else if (action.equalsIgnoreCase("customerMostRevenue")){
            
            forward = REV_BY_CUST_MAX;
            request.setAttribute("customerMax", revDAO.getCustomerMaxRev());
            
        } else if (action.equalsIgnoreCase("bestSellers")){
            
            forward = BEST_SELLERS;
            request.setAttribute("bestSellers", itemDAO.getBestSellers());
            
        } else {
            forward = ERROR;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
