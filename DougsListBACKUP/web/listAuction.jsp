<%-- 
    Document   : listAuction
    Created on : Nov 21, 2015, 4:08:31 AM
    Author     : ShawnCruz
--%>

<%@page import="java.util.List"%>
<%@page import="doug.model.Employee"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
    <head>  
        <!--- Css/Fonts/Scripts --->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--- End CSS/Fonts/Scripts --->  
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">  
        <title>Ongoing Auctions</title>  
    </head>  
    <body>
        <nav class="navbar navbar-inverse">
            <div class ='nav-container'>
                <h1 class='main-title'>Doug's List <span class="glyphicon glyphicon-tag"></span> </h1>
                <div class='welcome-text'>
                    <div class='home'><a title='Home' href='index.jsp'><span class="glyphicon glyphicon-home"></span></a></div>
                    <div class='account'><a title='My Account' href='${destination}'><span class='glyphicon glyphicon-user'></span></a></div>
                    <div class='account'><a title='Logout' href='LogoutServlet'><span class='glyphicon glyphicon-log-out'></span></a></div>
                    <div class='account'><a title='Help' href='help.jsp'><span class='glyphicon glyphicon-question-sign'></span></a></div>
                    Hello, ${currentUser.firstName}                         
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="inner-container">
                <h5>All Auctions</h5>
                <table with="100%" border=1>
                    <thead>
                        <tr>
                            <th>Auction Id</th>
                            <th>Item Id</th>
                            <th>Monitor Id</th>
                            <th>Current Winner</th>
                            <th>Bit Increment</th>
                            <th>Current Bid</th>
                            <th>Finished</th>
                            <th>Reserve Price</th>
                            <th colspan=2>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${auctions}" var="auction">
                            <tr>        
                                <td><c:out value="${auction.auctionId}"/></td>
                                <td><c:out value="${auction.itemId}"/></td>
                                <td><c:out value="${auction.monitor}"/></td>
                                <td><c:out value="${auction.currentWinner}"/></td>
                                <td><c:out value="${auction.bidIncrement}"/></td>
                                <td><c:out value="${auction.currentBid}"/></td>
                                <td><c:out value="${auction.finished}"/></td>
                                <td><c:out value="${auction.reservePrice}"/></td>
                                <td><a href="AuctionServlet?action=recordSale&auctionId=<c:out value="${auction.auctionId}"/>">Record Sale</a>
                                <a href="AuctionServlet?action=viewAuction&auctionId=<c:out value="${auction.auctionId}"/>">View</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
